//
//  AppDelegate.swift
//  MyFarmStand
//
//  Created by Prabhat on 20/10/20.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces


import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import UserNotifications
import UserNotificationsUI

import Stripe


@main
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        /************************************************************************************************
                                                Development Key
        *************************************************************************************************/
        
//         STPPaymentConfiguration.shared().publishableKey = "pk_test_LnXQ5ClKG4l5TGEOKgI66fFJ00IMEzm1Ci"
        
        
        /************************************************************************************************
                                                Production Key
        *************************************************************************************************/
        
         STPPaymentConfiguration.shared().publishableKey = "pk_live_51IELOiIm7uwJT5TidlAlI5OkSkYEPkvbgt7MkmEMqP5GjC9V1mkNzIELi4Oe7R8VSyfe84R65fnNnwV1478NO3pl00xNpywPKK"

        
        
        GMSServices.provideAPIKey("AIzaSyAzLbrnMb_HagcXwSusuyGTKW3YsrQQERA")
        GMSPlacesClient.provideAPIKey("AIzaSyAzLbrnMb_HagcXwSusuyGTKW3YsrQQERA")
        
        IQKeyboardManager.shared.enable = true
        
        
        UNUserNotificationCenter.current().delegate = self
         if #available(iOS 10, *) {
             UNUserNotificationCenter.current().requestAuthorization(options:[.badge, .alert, .sound]){ granted, error in }
         } else {
             application.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
         }
         
         application.registerForRemoteNotifications()
         FirebaseApp.configure()
         Messaging.messaging().delegate = self
         
        // Messaging.messaging().shouldEstablishDirectChannel = true
        Messaging.messaging().shouldGroupAccessibilityChildren = true
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                                name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)

        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}


// Mark: Push notification
extension AppDelegate : MessagingDelegate {
    
    @objc func tokenRefreshNotification(notification: NSNotification) {
        
        if let token = Messaging.messaging().fcmToken {
            print("Refresh Token :-\(token)")
            
            UserDefaults.standard.set(token, forKey: "FCM_TOKEN")
            print(UserDefaults.standard.value(forKey: "FCM_Token")!)
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        debugPrint("didRegisterForRemoteNotificationsWithDeviceToken: DATA")
        let token = String(format: "%@", deviceToken as CVarArg)
        debugPrint("*** deviceToken: \(token)")
        Messaging.messaging().apnsToken = deviceToken
        print("Firebase Token:",InstanceID.instanceID().token)
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        
        print(userInfo)
        
        guard let json = userInfo[AnyHashable("aps")] as? [String: Any] else { return }
        
        
        print(json)
        
        if let notificationInfo = json["alert"] as? [String: Any] {
            guard let bodyInfo = notificationInfo["body"] as? String else { return }
            print(bodyInfo)
        }
        
        //If app is currently running on foreground.
        let state = UIApplication.shared.applicationState
        if state == .active {
//            setRootVC()
        }
        
        // Change this to your preferred presentation option
        completionHandler([.sound, .alert, .badge])
        print("User Info : \(notification.request.content.userInfo)")
        
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification info: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
         
        /*
         let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
          let vc = storyboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
          
          let navigationController = UINavigationController(rootViewController: vc)
          navigationController.navigationBar.isHidden = true
          let appdelegate = UIApplication.shared.delegate as! AppDelegate
          let vc2 = storyboard.instantiateViewController(withIdentifier: "MyGiftsBarVC") as! MyGiftsBarVC
         
         vc2.comeFrom = "notification"
          vc.navigationController?.pushViewController(vc2, animated: false)
          appdelegate.window!.rootViewController = navigationController
         */
    }
    
    

    
    /*
     func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
         
         debugPrint("--->messaging:\(messaging)")
         debugPrint("--->didReceive Remote Message:\(remoteMessage.appData)")
         guard let data =
             try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
             let prettyPrinted = String(data: data, encoding: .utf8) else { return }
         print("Received direct channel message:\n\(prettyPrinted)")
     }
     */
    
    
    
    func updateFirestorePushTokenIfNeeded() {
        if let token = Messaging.messaging().fcmToken {
            UserDefaults.standard.set(token, forKey: "FCM_Token")
            print(UserDefaults.standard.value(forKey: "FCM_Token") ?? "Nil")
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        updateFirestorePushTokenIfNeeded()
        if let token = Messaging.messaging().fcmToken {
            print("FCM_Token....\(token)")
            //            UserDefaults.standard.set(token, forKey: "FCM_Token")
            UserDefaults.standard.set(token, forKey: "FCM_TOKEN")
            print(UserDefaults.standard.value(forKey: "FCM_Token") ?? "Nil")
        }
    }
    
}
