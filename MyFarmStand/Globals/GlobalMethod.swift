//
//  GlobalMethod.swift
//  Art_B
//
//  Created by Ranjit on 24/05/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import Foundation
import UIKit

var CheckString = String()

let APP_NAME = "MyFarmStand"

let placeImage = UIImage(named: "image")
class GlobalMethod: NSObject {
    
    func isLoggedIn() -> Bool {
        let info = UserDefaults.standard.value(forKey: "")
        if info != nil {
            return true
        }
        return false
    }
    
    /*
     Image's Border Radius
     */
    
    func imgCircle( image: UIImageView, borderWidth: Int, masksToBounds: Bool, borderColor: UIColor, clipsToBounds: Bool) -> Void {
        image.layer.borderWidth = CGFloat(borderWidth)
        image.layer.masksToBounds = masksToBounds
        image.layer.borderColor = borderColor.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    
    func viewCircle(image: UIView, borderWidth: Int, masksToBounds: Bool, borderColor: UIColor, clipsToBounds: Bool) -> Void {
        image.layer.borderWidth = CGFloat(borderWidth)
        image.layer.masksToBounds = masksToBounds
        image.layer.borderColor = borderColor.cgColor
        image.layer.cornerRadius = image.frame.height/2
        image.clipsToBounds = true
    }
    
    func showAlert(title: String, message: String, vc: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {vc.present(alert, animated: true) }
    }
    
    func pushToVC(storyBoard: String, VCId: String, VC: UIViewController) {
        let story =  UIStoryboard.init(name: storyBoard, bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: VCId)
        VC.navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentToVC(storyBoard: String, VCId: String, VC: UIViewController) {
        let story =  UIStoryboard.init(name: storyBoard, bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: VCId)
        VC.present(vc, animated: true, completion: nil)
    }
    
    func UTCToLocal(dateStr:String) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MMM yyyy, HH:mm a"
        if let date = dateFormatterGet.date(from: dateStr){
            return dateFormatterPrint.string(from: date)
        }
        else {
            return "Error in dateFormat"
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
    class func alertForiPad() -> UIAlertController {
        if UIScreen.main.bounds.width > 414 {
           return UIAlertController(title: nil, message: nil, preferredStyle: .alert)
        }
        else {
            return UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        }
    }
    
    
    
    func removeKey() -> Void {
        UserDefaults.standard.removeObject(forKey: "session_id")
        UserDefaults.standard.removeObject(forKey: "User_Id")
        UserDefaults.standard.removeObject(forKey: "user_name")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "profile_image")
        
    }

    
    
}


