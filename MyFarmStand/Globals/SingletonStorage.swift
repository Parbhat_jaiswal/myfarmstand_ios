//
//  SingletonStorage.swift
//  AyMaEat
//
//  Created by Prabhat on 03/04/20.
//  Copyright © 2020 Lee Da Hang Pte Ltd. All rights reserved.
//

import Foundation

class SingletonStorage {
    
    var logInStatus: String?
    
    class var sharedManager: SingletonStorage {
        struct Static {
            static let instance = SingletonStorage()
        }
        return Static.instance
    }
}
