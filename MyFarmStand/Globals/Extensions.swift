//
//  Extensions.swift
//  Deal
//
//  Created by Ranjit on 18/07/19.
//  Copyright © 2019 RichestSoft. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView


extension Notification.Name {
    static let showRatingVC = Notification.Name("ShowRatingVC")

}



//MARK:- EXTENSION TO UITEXT FIELD.

extension String {
    
    var isValidEmail: Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: self)
    }
    
}


extension UITextField {
    
    //MARK:- Email Validation.

    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self.text!)
    }
   
}//..

