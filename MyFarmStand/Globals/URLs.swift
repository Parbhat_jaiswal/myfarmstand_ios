//
//  URLs.swift
//  WineApp
//
//  Created by Prabhat on 28/02/20.
//  Copyright © 2020 Lee Da Hang Pte Ltd. All rights reserved.
//

import Foundation

// var BASE_URL = "http://167.172.209.57/foodapp-test/api/v1/"


var BASE_URL = "https://myteamdevops.com/FoodAppNew/api/v1/"
// var BASE_URL = "http://testingg.fwphotographers.com/api/v1/"
var WEB_VIEW_BASE_URL = ""
var IMG_BASE_URL = ""


/**********************************************************************/
                    /* START MAIN API'S END POINTS */
/**********************************************************************/

var TERM_CONDITION = WEB_VIEW_BASE_URL + "https://myteamdevops.com/FoodAppNew/admin/terms"
var PRIVACY_POLICY = WEB_VIEW_BASE_URL + "https://myteamdevops.com/FoodAppNew/admin/privacy-policy"
var ABOUT_US = WEB_VIEW_BASE_URL + "about-us"

var LOGIN = BASE_URL + "user/login"
var REGISTER = BASE_URL + "user/register"
var FORGOT_PW = BASE_URL + "user/forgot_password"
var CHANGE_PASSWORD = BASE_URL + "user/change_password"
var EDIT_PROFILE = BASE_URL + "user/edit_profile"
var GET_PROFILE = BASE_URL + "user/get_profile"
var LOGOUT = BASE_URL + "user/logout"
var MY_PRODUCE = BASE_URL + "user/my_produce"


var CATEGORY = BASE_URL + "products/categories"
var PRODUCT_BY_CATEGORY = BASE_URL + "products/product_by_category"
var POPULAR_PRODUCTS = BASE_URL + "products/popular_products"
var SEARCH_PRODUCTS = BASE_URL + "products/search"
var ADD_PRODUCTS = BASE_URL + "products/sell_product"

var DELETE_PRODUCTS = BASE_URL + "user/deleteAddedProduct"

var ADD_REMOVE_FAVORITE = BASE_URL + "products/add_or_remove_to_favorites"
var FAVORITES = BASE_URL + "user/favorites"

var CHAT_USERS = BASE_URL + "chats"
var RECIEVE_MESSAGE = BASE_URL + "chat/receive"
var SEND_MESSAGE = BASE_URL + "chat/send"
var BLOCK_USER = BASE_URL + "user/user_block"
var BLOCK_USER_LIST = BASE_URL + "user/user_block_list"

var DELETE_CHAT = BASE_URL + ""

var NOTIFICATIONS = BASE_URL + "user/getNotification"
var DELETE_NOTIFICATIONS = BASE_URL + "user/removeNotification"
var PURCHASED_HISTORY = BASE_URL + "user/getPurchasedProduct"




var ADD_CART = BASE_URL + "user/addCart"
var SHOW_CART = BASE_URL + "user/getProductCart"
var ADD_CART_PLUSE_MINUSE = BASE_URL + "user/updateCart"
var REMOVE_CART = BASE_URL + "user/removeCart"

var ORDER_PROCESS = BASE_URL + "user/makePayment"
var MAKE_PAYMENT = BASE_URL + "userpay"



