//
//  PopularModel.swift
//  MyFarmStand
//
//  Created by Prabhat on 11/01/21.
//

import Foundation
import SwiftyJSON

/*
{
    "product_id": 6,
    "posted_by_id": 1,
    "posted_by_name": "test2",
    "posted_by_profile_image": "http://testingg.fwphotographers.com/public/uploads/profile/default.jpg",
    "category": 1,
    "title": "Product 5",
    "description": "Product 3 description",
    "detail": [],
    "price": 10.300000000000000710542735760100185871124267578125,
    "currency": "$",
    "created_at": "13 hours before",
    "latitude": 30.699999999999999289457264239899814128875732421875,
    "longitude": 76.719999999999998863131622783839702606201171875,
    "images": [
    {
    "id": 3,
    "product_id": 0,
    "advertisement_id": 6,
    "image": "BIAw4WHNDL_Screenshot from 2021-01-10 20-09-10.png",
    "created_at": "2021-01-10 16:46:23",
    "updated_at": "2021-01-10 16:46:23"
    }
    ],
    "images_base_path": "http://testingg.fwphotographers.com/public/uploads/products/",
    "favourites": 2,
    "favorite_date": "2021-01-10 11:56:14",
    "is_favourite": 1,
    "is_sold": 0
}
 
*/


class PopularModel: NSObject {
    
    var product_id: Int?
    var posted_by_id: Int?
    var posted_by_name: String?
    var posted_by_profile_image: String?
    var category: Int?
    var title: String?
    var desc: String?
//    var detail: String?
    var price: Double?
    var currency: String?
    var created_at: String?
    var latitude: Double?
    var longitude: Double?
    
    var images: [PopularImgModel] = []
    
    var images_base_path: String?
    var favourites: Int?
    var favorite_date: String?
    var is_favourite: Int?
    var is_sold: Int?
    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else {
                return
        }
        
        self.product_id = jsonInfo["product_id"]?.intValue
        self.posted_by_id = jsonInfo["posted_by_id"]?.intValue
        self.posted_by_name = jsonInfo["posted_by_name"]?.stringValue
        self.posted_by_profile_image = jsonInfo["posted_by_profile_image"]?.stringValue
        self.category = jsonInfo["category"]?.intValue
        self.title = jsonInfo["title"]?.stringValue
        self.desc = jsonInfo["description"]?.stringValue
//        self.detail = jsonInfo["detail"]?.stringValue
        self.price = jsonInfo["price"]?.doubleValue
        self.currency = jsonInfo["currency"]?.stringValue
        self.created_at = jsonInfo["created_at"]?.stringValue
        self.latitude = jsonInfo["latitude"]?.doubleValue
        self.longitude = jsonInfo["longitude"]?.doubleValue
        
        if let data = jsonInfo["images"]?.arrayValue {
            for i in data  {
                self.images.append(PopularImgModel.init(info: JSON(i)))
            }
        }
        
        self.images_base_path = jsonInfo["images_base_path"]?.stringValue
        self.favourites = jsonInfo["favourites"]?.intValue
        self.favorite_date = jsonInfo["favorite_date"]?.stringValue
        self.is_favourite = jsonInfo["is_favourite"]?.intValue
        self.is_sold = jsonInfo["is_sold"]?.intValue

    }
    
}


/*
 
{
    "id": 3,
    "product_id": 0,
    "advertisement_id": 6,
    "image": "BIAw4WHNDL_Screenshot from 2021-01-10 20-09-10.png",
    "created_at": "2021-01-10 16:46:23",
    "updated_at": "2021-01-10 16:46:23"
}
 
*/


class PopularImgModel: NSObject {
    var id: Int?
    var product_id: Int?
    var advertisement_id: Int?
    var image: String?
    var created_at: String?
    var updated_at: String?
    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        guard let jsonInfo = info?.dictionary
            else {
                return
        }
        self.id = jsonInfo["id"]?.intValue
        self.product_id = jsonInfo["product_id"]?.intValue
        self.advertisement_id = jsonInfo["advertisement_id"]?.intValue
        self.image = jsonInfo["image"]?.stringValue
        self.created_at = jsonInfo["created_at"]?.stringValue
        self.updated_at = jsonInfo["updated_at"]?.stringValue
    }
    
}
