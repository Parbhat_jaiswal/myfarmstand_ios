//
//  CartModel.swift
//  AyMaEat
//
//  Created by Prabhat on 14/04/20.
//  Copyright © 2020 Lee Da Hang Pte Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

/*
 
{
    "cart_id": 5,
    "productName": "Product 3",
    "actualPrice": "10.30",
    "quantity": 4,
    "totalPrice": "41.20"
}

 */

class CartModel: NSObject {
    
    var cart_id: Int?
    var productName: String?
    var actualPrice: String?
    var quantity: Int?
    var totalPrice: String?

    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
                
        cart_id = jsonInfo["cart_id"]?.intValue
        productName = jsonInfo["productName"]?.stringValue
        actualPrice = jsonInfo["actualPrice"]?.stringValue
        quantity = jsonInfo["quantity"]?.intValue
        totalPrice = jsonInfo["totalPrice"]?.stringValue
    }
    
}
