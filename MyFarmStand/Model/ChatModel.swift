//
//  ChatVC.swift
//  Gays4gals
//
//  Created by Prabhat on 18/10/20.
//  Copyright © 2020 unwrapsolutions. All rights reserved.
//

import Foundation
import SwiftyJSON


/*
 
 {
     "title": "15 Oct, 2020",
     "data": [
     {
     "id": 2,
     "message": "Hi",
     "from_user_id": 3,
     "to_user_id": 2,
     "sent_time": "06:18 pm"
     }
     ]
 }
*/

class ChatModel: NSObject {
    var titleDate: String?
    var msgArr: [MessageListModel] = []
        
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        titleDate = jsonInfo["title"]?.stringValue
        
        if let data = jsonInfo["data"]?.arrayValue {
            for i in data  {
                msgArr.append(MessageListModel.init(info: JSON(i)))
            }
        }

    }
    
}


class MessageListModel: NSObject {
    
    var id: String?
    var message: String?
    var from_user_id: Int?
    var to_user_id: Int?
    var sent_time: String?
    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        id = jsonInfo["id"]?.stringValue
        message = jsonInfo["message"]?.stringValue
        from_user_id = jsonInfo["from_user_id"]?.intValue
        to_user_id = jsonInfo["to_user_id"]?.intValue
        sent_time = jsonInfo["sent_time"]?.stringValue

    }
    
}



