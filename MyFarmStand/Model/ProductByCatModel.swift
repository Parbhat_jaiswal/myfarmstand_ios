//
//  ProductByCatModel.swift
//  MyFarmStand
//
//  Created by Prabhat on 11/01/21.
//

import Foundation
import SwiftyJSON


class ProductByCatModel: NSObject {
    
    var product_id: Int?
    var posted_by_id: Int?
    var posted_by_name: String?
    var posted_by_profile_image: String?
    var category: Int?
    var title: String?
    var desc: String?
//    var detail: String?
    var price: String?
    var currency: String?
    var created_at: String?
    var latitude: String?
    var longitude: String?
    
    var images: [ProductByCatImgModel] = []
    
    var images_base_path: String?
    var favourites: Int?
    var favorite_date: String?
    var is_favourite: Int?
    var is_sold: Int?
    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else {
                return
        }
        
        self.product_id = jsonInfo["product_id"]?.intValue
        self.posted_by_id = jsonInfo["posted_by_id"]?.intValue
        self.posted_by_name = jsonInfo["posted_by_name"]?.stringValue
        self.posted_by_profile_image = jsonInfo["posted_by_profile_image"]?.stringValue
        self.category = jsonInfo["category"]?.intValue
        self.title = jsonInfo["title"]?.stringValue
        self.desc = jsonInfo["description"]?.stringValue
//        self.detail = jsonInfo["detail"]?.stringValue
        self.price = jsonInfo["price"]?.stringValue
        self.currency = jsonInfo["currency"]?.stringValue
        self.created_at = jsonInfo["created_at"]?.stringValue
        self.latitude = jsonInfo["latitude"]?.stringValue
        self.longitude = jsonInfo["longitude"]?.stringValue
        
        if let data = jsonInfo["images"]?.arrayValue {
            for i in data  {
                self.images.append(ProductByCatImgModel.init(info: JSON(i)))
            }
        }
        
        self.images_base_path = jsonInfo["images_base_path"]?.stringValue
        self.favourites = jsonInfo["favourites"]?.intValue
        self.favorite_date = jsonInfo["favorite_date"]?.stringValue
        self.is_favourite = jsonInfo["is_favourite"]?.intValue
        self.is_sold = jsonInfo["is_sold"]?.intValue

    }
    
}


/*
 
{
    "id": 3,
    "product_id": 0,
    "advertisement_id": 6,
    "image": "BIAw4WHNDL_Screenshot from 2021-01-10 20-09-10.png",
    "created_at": "2021-01-10 16:46:23",
    "updated_at": "2021-01-10 16:46:23"
}
 
*/


class ProductByCatImgModel: NSObject {
    var id: Int?
    var product_id: Int?
    var advertisement_id: Int?
    var image: String?
    var created_at: String?
    var updated_at: String?
    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else {
                return
        }
        
        self.id = jsonInfo["id"]?.intValue
        self.product_id = jsonInfo["product_id"]?.intValue
        self.advertisement_id = jsonInfo["advertisement_id"]?.intValue
        self.image = jsonInfo["image"]?.stringValue
        self.created_at = jsonInfo["created_at"]?.stringValue
        self.updated_at = jsonInfo["updated_at"]?.stringValue
        
    }
}
