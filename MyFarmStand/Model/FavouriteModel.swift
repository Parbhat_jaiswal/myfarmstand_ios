//
//  FavouriteModel.swift
//  MyFarmStand
//
//  Created by Prabhat on 11/01/21.
//

import Foundation
import SwiftyJSON

/*

{
    "product_id": 5,
    "posted_by_id": 3,
    "posted_by_name": "Demo",
    "posted_by_profile_image": "http://testingg.fwphotographers.com/public/uploads/profile/default.jpg",
    "category": 1,
    "title": "Product 3",
    "description": "Product 3 description",
    "price": "10.30",
    "currency": "$",
    "created_at": "11 hours before",
    "latitude": "0.00",
    "longitude": "76.72",
    "images": [
    {
    "id": 2,
    "product_id": 0,
    "advertisement_id": 5,
    "image": "6KiZHQg1TF_Screenshot 2021-01-10 at 6.28.13 PM.png",
    "created_at": "2021-01-10 16:45:47",
    "updated_at": "2021-01-10 16:45:47"
    }
    ],
    "images_base_path": "http://testingg.fwphotographers.com/public/uploads/products/",
    "favorites": 2,
    "favorite_date": "2021-01-10 11:56:17",
    "is_favourite": 1,
    "is_sold": 0
}

*/

class FavouriteModel: NSObject {
    
    var product_id: Int?
    var posted_by_id: Int?
    var posted_by_name: String?
    var posted_by_profile_image: String?
    var category: String?
    var title: String?
    var desc: String?
    var price: String?
    var currency: String?
    var created_at: String?
    var latitude: String?
    var longitude: String?
    var images_base_path: String?
    var favorites: Int?
    var favorite_date: String?
    var is_favourite: Int?
    var is_sold: Int?
    
    var images: [FavImgModel] = []
    
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        self.product_id = jsonInfo["product_id"]?.intValue
        self.posted_by_id = jsonInfo["posted_by_id"]?.intValue
        self.posted_by_name = jsonInfo["posted_by_name"]?.stringValue
        self.posted_by_profile_image = jsonInfo["posted_by_profile_image"]?.stringValue
        self.category = jsonInfo["category"]?.stringValue
        self.title = jsonInfo["title"]?.stringValue
        self.desc = jsonInfo["description"]?.stringValue
        self.price = jsonInfo["price"]?.stringValue
        self.currency = jsonInfo["currency"]?.stringValue
        self.created_at = jsonInfo["created_at"]?.stringValue
        self.latitude = jsonInfo["latitude"]?.stringValue
        self.longitude = jsonInfo["longitude"]?.stringValue
        self.images_base_path = jsonInfo["images_base_path"]?.stringValue
        self.favorites = jsonInfo["favorites"]?.intValue
        self.favorite_date = jsonInfo["favorite_date"]?.stringValue
        self.is_favourite = jsonInfo["is_favourite"]?.intValue
        self.is_sold = jsonInfo["is_sold"]?.intValue
        
        
        if let data = jsonInfo["images"]?.arrayValue {
            for i in data  {
                images.append(FavImgModel.init(info: JSON(i)))
            }
        }

    }
    
}


/*
{
    "id": 2,
    "product_id": 0,
    "advertisement_id": 5,
    "image": "6KiZHQg1TF_Screenshot 2021-01-10 at 6.28.13 PM.png",
    "created_at": "2021-01-10 16:45:47",
    "updated_at": "2021-01-10 16:45:47"
}
*/


class FavImgModel: NSObject {
    var id: Int?
    var product_id: Int?
    var advertisement_id: Int?
    var image: String?
    var created_at: String?
    var updated_at: String?
            
    override init() {
        super.init()
    }
    
    init(info : JSON?){
        super.init()
        fillInfo(info: info)
    }
    
    private func fillInfo(info : JSON?) {
        
        guard let jsonInfo = info?.dictionary
            else{
                return
        }
        
        self.id = jsonInfo["id"]?.intValue
        self.product_id = jsonInfo["product_id"]?.intValue
        self.advertisement_id = jsonInfo["advertisement_id"]?.intValue
        self.image = jsonInfo["image"]?.stringValue
        self.created_at = jsonInfo["created_at"]?.stringValue
        self.updated_at = jsonInfo["updated_at"]?.stringValue

    }

}

