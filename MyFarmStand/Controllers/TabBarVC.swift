//
//  TabBarVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 20/10/20.
//

import UIKit

class TabBarVC: UITabBarController, UITabBarControllerDelegate {

    fileprivate var popUpView = UIViewController()

    override func viewDidLoad() {
        self.delegate = self
        super.viewDidLoad()

    }
    
    // UITabBarControllerDelegate
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        var ind: Int = 0
        
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {

        } else {
            if tabBarController.selectedIndex == 0 {
                ind = 0
            } else if tabBarController.selectedIndex == 1 {
                ind = 1
            } else {
               closeView()
            }
            tabBarController.selectedIndex = ind
        }
        

    }
    
}

extension TabBarVC: AuthenticationViewProtocal {
    
    func closeView() {
        if !(popUpView.isViewLoaded) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "AuthenticationPopUpID") as? AuthenticationPopUp {
                loadVC.delegate = self
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                popUpView.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.popUpView.view.alpha = 1
                }, completion: nil)
                popUpView = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.popUpView.view.alpha = 0
            }, completion: nil)
            popUpView.view.removeFromSuperview()
            popUpView = UIViewController()
        }
        
    }
    
}
