//
//  PurchaseHistoryVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 05/02/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class PurchaseHistoryVC: UIViewController {
    
    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    var dataSource = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        getList()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PurchaseHistoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseHistoryCell", for: indexPath) as! PurchaseHistoryCell
        let index = JSON(dataSource[indexPath.row])
        cell.name.text = index["productName"].stringValue.capitalized
        cell.item.text = "Item: \(index["quantity"].stringValue)"
        cell.price.text = "Item Price: $\(index["totalPrice"].stringValue)"
        return cell
    }
    
}

extension PurchaseHistoryVC {
    private func getList() -> Void {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""
        let id = UserDefaults.standard.string(forKey: "User_Id") ?? ""
        let parameters = [
            "user_id":id,
            "session_id": session,
        ]
        print("SESSION =>>",session)
        Loader.shared.show()
        
        AF.request(PURCHASED_HISTORY, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")

        if rss["status"].intValue == 200  {
            dataSource.removeAllObjects()
            for i in rss["cart_details"].arrayValue {
                dataSource.add(i)
            }
            
            DispatchQueue.main.async(execute: {
                self.tbl.isHidden = self.dataSource.count == 0
                self.noDataLbl.isHidden = !(self.dataSource.count == 0)
                self.tbl.reloadData()
            })

        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
}

class PurchaseHistoryCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var item: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
}
