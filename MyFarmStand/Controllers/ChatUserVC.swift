//
//  ChatUserVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 22/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class ChatUserVC: UIViewController {

    @IBOutlet weak var noDataLbl: UILabel!
    @IBOutlet weak var chatUserTable: UITableView!
    var chatUser = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchChatList(isLoading: true)
    }
    
}

extension ChatUserVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return chatUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatUserCell_Id", for: indexPath) as! ChatUserCell
        let index = JSON(chatUser[indexPath.row])
        
//        let prodData = JSON(index["product_detail"].dictionaryValue)
//        let imgData = prodData["images"].arrayValue
//        if imgData.count != 0 {
//            let img = imgData[0].dictionaryValue
//            let imgName = img["image"]?.stringValue ?? ""
//            let imageUrl = index["image_base_path"].stringValue + imgName
//            print(imageUrl)
//            if let urlString = imageUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
//                cell.userImg?.kf.indicatorType = .activity
//                cell.userImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
//            }
//        }

        let imgData = index["profile_image"].stringValue

        if let urlString = imgData.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            cell.userImg?.kf.indicatorType = .activity
            cell.userImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
        }
        
        cell.usernameLbl.text = index["name"].stringValue.capitalized
        cell.badgeLbl.text = String(index["msg_count"].intValue)
        cell.timeLbl.text = index["time"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let index = JSON(chatUser[indexPath.row])

        let vc = story.instantiateViewController(withIdentifier: "ChatVC_Id") as! ChatVC
        vc.userId = index["user_id"].stringValue
        vc.userName = index["name"].stringValue
        vc.prodId = "\(index["product_id"].intValue)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func blockAction(_ sender: UIButton) {
//        let dialogMessage = UIAlertController(title: APP_NAME, message: "Are you sure you want to block?", preferredStyle: .alert)
//        // Create OK button with action handler
//        let ok = UIAlertAction(title: "YES", style: .destructive, handler: { (action) -> Void in
//            self.blockByApi(index: sender.tag, id: JSON(self.dataSource[sender.tag])["id"].stringValue, userName: JSON(self.dataSource[sender.tag])["name"].stringValue)
//        })
//        // Create Cancel button with action handlder
//        let cancel = UIAlertAction(title: "NO", style: .cancel) { (action) -> Void in
//            print("Cancel button tapped")
//        }
//        //Add OK and Cancel button to dialog message
//        dialogMessage.addAction(ok)
//        dialogMessage.addAction(cancel)
//        // Present dialog message to user
//        self.present(dialogMessage, animated: true, completion: nil)
    }
}

extension ChatUserVC {
    //fetch Favt
    private func fetchChatList(isLoading: Bool) -> Void {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        let parameters = [
            "session_id": session,
        ]
        print("SESSION =>>",session)
        print("CHAT_USERS =>>",CHAT_USERS)
        Loader.shared.show()
        
        AF.request(CHAT_USERS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")

        if rss["status"].intValue == 200 {
            
            chatUser.removeAllObjects()
            
            for i in rss["chats"].arrayValue {
                chatUser.add(i)
            }
            
            DispatchQueue.main.async(execute: {
                self.chatUserTable.reloadData()
                self.chatUserTable.isHidden = self.chatUser.count == 0
                self.noDataLbl.isHidden = !(self.chatUser.count == 0)
            })

        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
}


class ChatUserCell: UITableViewCell {
    
    @IBOutlet weak var userImg: DesignableImage!
    
    @IBOutlet weak var badgeLbl: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var msgLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}




