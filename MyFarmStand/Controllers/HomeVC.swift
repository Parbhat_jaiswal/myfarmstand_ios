//
//  HomeVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 22/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage
import Kingfisher


class HomeVC: UIViewController {

    @IBOutlet weak var favtLblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var favtCollectionHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var popularLblHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var popularCollectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var favoriteProductCollection: UICollectionView!
    @IBOutlet weak var popularProductCollection: UICollectionView!
    @IBOutlet weak var emptyLbl: UILabel!
    
    var favProducts = [FavouriteModel]()
    var popularProducts = [PopularModel]()
    
    fileprivate var popUpView = UIViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        favtLblHeightConstraint.constant = 0
        favtCollectionHeightConstraint.constant = 0
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        fetchFavProdList(isLoading: true)
        fetchPopularProdList(isLoading: true)
    }
    
    @IBAction func searchAction(_ sender: Any) {
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "SearchVC_Id", VC: self)
        } else {
            self.closeView()
        }
    }
    
}

extension HomeVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView == favoriteProductCollection ? favProducts.count : popularProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == favoriteProductCollection {    //favoriteProductCollection
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteProductCell_Id", for: indexPath) as! FavoriteProductCell
            let index = favProducts[indexPath.row]
            cell.setupCell(info: index)
            cell.heartBtn.tag = indexPath.row
            cell.heartBtn.addTarget(self, action: #selector(heartButtonPressed(_:)), for: .touchUpInside)
            
            cell.addBtn.tag = indexPath.row
            cell.addBtn.addTarget(self, action: #selector(addBtnAction(_:)), for: .touchUpInside)
            
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction1(_:)), for: .touchUpInside)

            
            if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String {
                if index.posted_by_id == Int(userId) {
                    cell.deleteBtn.isHidden = false
                    cell.btnView.isHidden = true
                } else {
                    cell.deleteBtn.isHidden = true
                    cell.btnView.isHidden = false
                }
            }
            
            return cell
        } else {    //PopularProductCell
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularProductCell_Id", for: indexPath) as! PopularProductCell
            let index = popularProducts[indexPath.row]
            cell.setupCell(info: index)
            
            cell.addBtn.tag = indexPath.row
            cell.addBtn.addTarget(self, action: #selector(addBtnAction(_:)), for: .touchUpInside)
            
            
            cell.deleteBtn.tag = indexPath.row
            cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction2(_:)), for: .touchUpInside)

            if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
                if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String {
                    if index.posted_by_id == Int(userId) {
                        cell.deleteBtn.isHidden = false
                        cell.btnView.isHidden = true
                    } else {
                        cell.deleteBtn.isHidden = true
                        cell.btnView.isHidden = false
                    }
                }
            } else {
                cell.deleteBtn.isHidden = true
            }
            
            return cell
        }
    }

    @objc func deleteBtnAction1(_ sender: UIButton) {
        let index = favProducts[sender.tag]

        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to delete?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (alert: UIAlertAction!) -> Void in
            self.deleteById(id: index.product_id ?? 0, index: sender.tag, type: "1")
        }
        let no = UIAlertAction(title: "No", style: .default) { (alert: UIAlertAction!) -> Void in
            
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)
    }
    
    @objc func deleteBtnAction2(_ sender: UIButton) {
        let index = popularProducts[sender.tag]
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to delete?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (alert: UIAlertAction!) -> Void in
            self.deleteById(id: index.product_id ?? 0, index: sender.tag, type: "2")
        }
        let no = UIAlertAction(title: "No", style: .default) { (alert: UIAlertAction!) -> Void in
            
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)

    }
    
    @objc func heartButtonPressed(_ sender: UIButton) {
        let index = favProducts[sender.tag]
        addRemoveFromFav(id: "\(index.product_id!)", favouriteStoreIndex:sender.tag)
    }
    
    @objc func addBtnAction(_ sender: UIButton) {
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            let index = popularProducts[sender.tag]
            addToCartByApi(user_id: index.posted_by_id!, product_id: index.product_id!, product_actual_price: "\(index.price!)", foodName: index.title!)
        } else {
            self.closeView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView == favoriteProductCollection ? CGSize(width: 338, height: 403) : CGSize(width: 330, height: 275)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == favoriteProductCollection {    //favoriteProductCollection
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "ProductDetailVC_Id") as! ProductDetailVC
            vc.homeFavInfo = favProducts[indexPath.row]
            print(favProducts)
            self.navigationController?.pushViewController(vc, animated: true)
        } else{
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "ProductDetailVC_Id") as! ProductDetailVC
            vc.popularProductsInfo = popularProducts[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
}

extension HomeVC {
    
    private func deleteById(id: Int, index: Int, type: String) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        guard let userId = UserDefaults.standard.value(forKey: "User_Id") as? String else { return }

        let parameters = [
            "session_id":token,
            "user_id": userId,
            "product_id":id
        ]
        as [String : Any]
        
        print(parameters)
        
        Loader.shared.show()
        
        AF.request(DELETE_PRODUCTS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(JSON(res))
                
                if JSON(res)["status"].intValue == 200 {
                    DispatchQueue.main.async(execute: {
                        if type == "1" {
                            self.favProducts.remove(at: index)
                            self.favoriteProductCollection.reloadData()
                        } else {
                            self.popularProducts.remove(at: index)
                            self.popularProductCollection.reloadData()
                        }
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["error_description"].stringValue, vc: self)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["error_description"].stringValue, vc: self)
                    })

                }
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}

//MARK: - Declaration of API Methods
extension HomeVC {
    //fetch Favt
    private func fetchFavProdList(isLoading: Bool) -> Void {
        
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
         
        let parameters = [
            "session_id": session,
        ]
        
        Loader.shared.show()
        
        AF.request(FAVORITES, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")

        if rss["message"].stringValue == "Favourites List Fetched"  {
            favProducts.removeAll()
            for i in rss["data"].arrayValue {
                favProducts.append(FavouriteModel.init(info: JSON(i)))
            }
            
            DispatchQueue.main.async(execute: {
                self.favtLblHeightConstraint.constant = self.favProducts.count == 0 ? 0 : 25
                self.favtCollectionHeightConstraint.constant = self.favProducts.count == 0 ? 0 : 400
                self.favoriteProductCollection.reloadData()
            })

        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
    //Fetch Popular
    private func fetchPopularProdList(isLoading: Bool) -> Void {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""
        let parameters = [
            "session_id": session
        ]

        Loader.shared.show()
        
        AF.request(POPULAR_PRODUCTS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):

                if JSON(res)["success"].stringValue == "Popular products list" {
                    self.popularProducts.removeAll()
                    for i in JSON(res)["data"].arrayValue {
                        self.popularProducts.append(PopularModel.init(info: JSON(i)))
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.popularProductCollection.reloadData()
                        self.popularLblHeightConstraint.constant = self.popularProducts.count == 0 ? 0 : 25
                        self.popularCollectionHeightConstraint.constant = self.popularProducts.count == 0 ? 0 : 276
                    })
                }
                else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["message"].string ?? "Please try after sometime!", vc: self)
                    })
                }
                
                DispatchQueue.main.async {
                    Loader.shared.hide()
                }

                if JSON(res)["error_description"].stringValue == "Your login session has been expired." {
                    GlobalMethod.init().removeKey()
                    
                    let alert = UIAlertController(title: "Success", message: "You have been successfully logged out!", preferredStyle: .alert)
                    let ok = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
                        let controllers : Array = self.navigationController!.viewControllers
                        self.navigationController!.popToViewController(controllers[0], animated: true)
                    }
                    alert.addAction(ok)
                    self.present(alert, animated: true, completion:nil)

                }
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    
}

extension HomeVC {
    func addRemoveFromFav(id: String, favouriteStoreIndex:Int) {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        let parameters = [
            "session_id": session,
            "product_id" : id
        ]
        
        Loader.shared.show()
        
        AF.request(ADD_REMOVE_FAVORITE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):

                if  JSON(res)["message"].stringValue != "Removed from Favorites" {

                    // Favourite
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Store favourite successfully!", vc: self)

                    let indexPath = IndexPath(row:favouriteStoreIndex, section: 0)
                    let cell = self.favoriteProductCollection.cellForItem(at: indexPath) as! FavoriteProductCell
                    cell.heartBtn.setImage(UIImage(named: "red_heart"), for: .normal)

                } else {
                    // Unfavourite
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Store unfavourite successfully!", vc: self)

                    let indexPath = IndexPath(row: favouriteStoreIndex, section: 0)
                    let cell = self.favoriteProductCollection.cellForItem(at: indexPath) as! FavoriteProductCell

                    cell.heartBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
                }
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}

extension HomeVC {
    
    func addToCartByApi(user_id: Int, product_id: Int, product_actual_price: String, foodName: String) -> Void {
        
        guard let session = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        
        let params = [
            "session_id":session,
            "user_id":user_id,
            "product_id":product_id,
            "product_actual_price":product_actual_price,
            "product_quantity":"1",
            "product_total_price":product_actual_price,
            "is_show":"1"
        ] as [String : Any]
        
        print(params)
        
        Loader.shared.show()
        //        AF.request(ADD_CART, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
        
        print(ADD_CART)
        print(params)
        
        AF.request(ADD_CART, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            print("Server Responce :-> ", response)
            
            switch response.result {
            case .success (let res):
                
                print("Server Responce :-> ", JSON(res))
                
                let JSONInfo = JSON(res)
                
                if JSONInfo["message"].stringValue == "Cart updated." {
                    
                    let myalert = UIAlertController(title: APP_NAME, message: "\(foodName.capitalized) successfully added in your cart!", preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        
                        //                        let story =  UIStoryboard.init(name: "User", bundle: nil)
                        //                        let vc = story.instantiateViewController(withIdentifier: "CartVC_Id") as! CartVC
                        //                        vc.isComeFrom = "FoodDetails"
                        //                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    })
                    self.present(myalert, animated: true)
                    
                } else if JSONInfo["error_description"].stringValue == "Product added in cart successfully." {
                    
                    let myalert = UIAlertController(title: APP_NAME, message: "\(foodName.capitalized) successfully added in your cart!", preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        
                        //                        let story =  UIStoryboard.init(name: "User", bundle: nil)
                        //                        let vc = story.instantiateViewController(withIdentifier: "CartVC_Id") as! CartVC
                        //                        vc.isComeFrom = "FoodDetails"
                        //                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    })
                    self.present(myalert, animated: true)
                    
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSONInfo["message"].string ?? "Please try again latter!", vc: self)
                    })
                }
            
                
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}


extension HomeVC: AuthenticationViewProtocal {
    
    func closeView() {
        if !(popUpView.isViewLoaded) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "AuthenticationPopUpID") as? AuthenticationPopUp {
                loadVC.delegate = self
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                popUpView.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.popUpView.view.alpha = 1
                }, completion: nil)
                popUpView = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.popUpView.view.alpha = 0
            }, completion: nil)
            popUpView.view.removeFromSuperview()
            popUpView = UIViewController()
        }
        
    }
    
}


//MARK: - Declaration of CollectionView Cell
class FavoriteProductCell: UICollectionViewCell {
    
    @IBOutlet weak var btnView: DesignableView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productDescLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var heartBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    
    func setupCell(info: FavouriteModel) {
        if info.images.count != 0 {
            let imgUrl = info.images_base_path! + info.images[0].image!
            if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                productImg?.kf.indicatorType = .activity
                productImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
            }
        }
        productNameLbl.text = info.title
        productPriceLbl.text = info.currency! + info.price!
        productDescLbl.text = info.desc
        
        if info.is_favourite == 1 {
            heartBtn.setImage(UIImage(named: "red_heart"), for: .normal)
        } else {
            heartBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
        }
    }
    
}

class PopularProductCell: UICollectionViewCell {
    
    @IBOutlet weak var btnView: DesignableView!
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!

    
    var addBtnClosure : ((PopularProductCell) -> Void)?
    
    @IBAction func addButtonAction(_ sender: UIButton) {
        addBtnClosure?(self)
    }
    
    func setupCell(info: PopularModel) {
        
        if info.images.count != 0 {
            print(info.images[0])
            
            let imgUrl = info.images_base_path! + info.images[0].image!
            if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                productImg?.kf.indicatorType = .activity
                productImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
            }
        }
        
        productNameLbl.text = info.title
        productPriceLbl.text = info.currency! + "\(info.price!)"

    }
}


