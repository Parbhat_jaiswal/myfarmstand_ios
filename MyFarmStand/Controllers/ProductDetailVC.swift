//
//  ProductDetailVC.swift
//  MyFarmStand
//
//  Created by Ankit Ahluwalia on 20/11/20.
//

import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage

class ProductDetailVC: UIViewController {

    @IBOutlet weak var prodImg: UIImageView!
    @IBOutlet weak var favtBtn: UIButton!
    @IBOutlet weak var prodName: UITextField!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var prodBGView: DesignableView!
    @IBOutlet weak var addressLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var howLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    
    var userData : Dictionary<String, Any>?
    
    var homeFavInfo: FavouriteModel?
    var popularProductsInfo: PopularModel?
    var productByCat: ProductByCatModel?
    
    
    fileprivate var popUpView = UIViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        homeFavInfo = nil
        popularProductsInfo = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setupHomeFavInfo()
        setupPopularProductsInfo()
        setupProductByCatInfo()
    }
    
    func setupHomeFavInfo() {
        if let info = homeFavInfo {
            self.usernameLbl.text = info.posted_by_name?.capitalized
            self.prodName.text = info.title?.capitalized
            if info.images.count != 0 {
                let imgUrl = info.images_base_path! + info.images[0].image!
                if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                    prodImg?.kf.indicatorType = .activity
                    prodImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "image"), options: [.transition(.fade(0.7))], progressBlock: nil)
                }
            }
            
            if let imgUrl = info.posted_by_profile_image {
                if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                    userImg?.kf.indicatorType = .activity
                    userImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
                }
            }
            
            if info.is_favourite == 1 {
                favtBtn.setImage(UIImage(named: "red_heart"), for: .normal)
            } else {
                favtBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
            }            
            
            self.addressLbl.text = info.created_at
            self.descLbl.text = info.desc
        }
    }

    
    func setupPopularProductsInfo() {
        if let info = popularProductsInfo {
            self.usernameLbl.text = info.posted_by_name?.capitalized
            self.prodName.text = info.title?.capitalized
            if info.images.count != 0 {
                let imgUrl = info.images_base_path! + info.images[0].image!
                if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                    prodImg?.kf.indicatorType = .activity
                    prodImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "image"), options: [.transition(.fade(0.7))], progressBlock: nil)
                }
            }
            
            if let imgUrl = info.posted_by_profile_image {
                if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                    userImg?.kf.indicatorType = .activity
                    userImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
                }
            }
            
            if info.is_favourite == 1 {
                favtBtn.setImage(UIImage(named: "red_heart"), for: .normal)
            } else {
                favtBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
            }
            
            self.addressLbl.text = info.created_at
            self.descLbl.text = info.desc
        }
    }
    
    func setupProductByCatInfo() {
        if let info = productByCat {
            self.usernameLbl.text = info.posted_by_name?.capitalized
            self.prodName.text = info.title?.capitalized
            
            if info.images.count != 0 {

                let imgUrl = info.images_base_path! + info.images[0].image!
                
                print(imgUrl)

                if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                    prodImg?.kf.indicatorType = .activity
                    prodImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "image"), options: [.transition(.fade(0.7))], progressBlock: nil)
                }
            }
            
            if let imgUrl = info.posted_by_profile_image {
                if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                    userImg?.kf.indicatorType = .activity
                    userImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
                }
            }
            
            if info.is_favourite == 1 {
                favtBtn.setImage(UIImage(named: "red_heart"), for: .normal)
            } else {
                favtBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
            }
            
            self.addressLbl.text = info.created_at
            self.descLbl.text = info.desc
        }
    }
    
    
    @IBAction func chatAction(_ sender: Any) {
        
        
        
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "ChatVC_Id") as! ChatVC
            if let info = homeFavInfo {
                vc.userId = "\(info.posted_by_id!)"
                vc.userName = info.posted_by_name!
                vc.prodId = "\(info.product_id!)"
            }
            if let info = popularProductsInfo {
                vc.userId = "\(info.posted_by_id!)"
                vc.userName = info.posted_by_name!
                vc.prodId = "\(info.product_id!)"
            }
            if let info = productByCat {
                vc.userId = "\(info.posted_by_id!)"
                vc.userName = info.posted_by_name!
                vc.prodId = "\(info.product_id!)"
            }
            
            self.navigationController?.pushViewController(vc, animated: true)

        } else {
            self.closeView()
        }
                
    }
    
    @IBAction func favtAction(_ sender: Any) {
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            if let info = homeFavInfo {
                addRemoveFromFav(id: "\(info.product_id!)")
            }
            
            if let info = popularProductsInfo {
                addRemoveFromFav(id: "\(info.product_id!)")
            }
            
            if let info = productByCat {
                addRemoveFromFav(id: "\(info.product_id!)")
            }

        } else {
            self.closeView()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension ProductDetailVC {
    
    func addRemoveFromFav(id: String) {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        let parameters = [
            "session_id": session,
            "product_id" : id
        ]
        
        Loader.shared.show()
        
        AF.request(ADD_REMOVE_FAVORITE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):

                if  JSON(res)["message"].stringValue != "Removed from Favorites" {
                    // Favourite
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Store favourite successfully!", vc: self)
                    self.favtBtn.setImage(UIImage(named: "red_heart"), for: .normal)

                } else {
                    // Unfavourite
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Store unfavourite successfully!", vc: self)
                    self.favtBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
                }
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}


extension ProductDetailVC: AuthenticationViewProtocal {
    
    func closeView() {
        if !(popUpView.isViewLoaded) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "AuthenticationPopUpID") as? AuthenticationPopUp {
                loadVC.delegate = self
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                popUpView.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.popUpView.view.alpha = 1
                }, completion: nil)
                popUpView = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.popUpView.view.alpha = 0
            }, completion: nil)
            popUpView.view.removeFromSuperview()
            popUpView = UIViewController()
        }
        
    }
    
}
