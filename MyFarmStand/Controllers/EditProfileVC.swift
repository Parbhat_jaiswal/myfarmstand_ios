//
//  EditProfileVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 23/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import GooglePlaces


class EditProfileVC: UIViewController {

    @IBOutlet weak var prodImg: UIImageView!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var locTF: UITextField!
    @IBOutlet weak var DOBTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    
    var address  = String()
    var lat:String = ""
    var long:String = ""
    var datePickerView = UIDatePicker()
    var userData : Dictionary<String, Any>? = nil
    
    var imagePicker = UIImagePickerController()
    var selectedImg: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self

        
        //Setting DatePicker
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.maximumDate = Date()
        datePickerView.addTarget(self, action: #selector(self.datePickerValueChanged(sender:)), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        setData()
    }
    
    private func setData() {
        self.usernameTF.text = UserDefaults.standard.string(forKey: "user_name") ?? ""
        self.emailTF.text = UserDefaults.standard.string(forKey: "email") ?? ""
        self.phoneTF.text = UserDefaults.standard.string(forKey: "phone") ?? ""
        self.DOBTF.text = UserDefaults.standard.string(forKey: "dob") ?? ""
        self.locTF.text = UserDefaults.standard.string(forKey: "loc") ?? ""
        
        let url = UserDefaults.standard.string(forKey: "profile_image") ?? ""
        if let urlString = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            prodImg?.kf.indicatorType = .activity
            prodImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
        }
        
        selectedImg = prodImg.image
        
    }
    
    @IBAction func openPickerAction(sender: UIButton) {
            DOBTF.inputView = datePickerView
            DOBTF.becomeFirstResponder()
    }
    
    @IBAction func uploadImage(_ sender: UIButton) {
        selectImage()
    }

    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        DOBTF.text = dateFormatter.string(from: sender.date)
    }
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func updateAction(_ sender: Any) {
        guard let name = usernameTF.text, name != ""
        else {
            GlobalMethod.init().showAlert(title: "Username", message: "Please enter your user name!", vc: self)
            return
        }
        
        
        guard let e = emailTF.text, e != ""
        else {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your email address!", vc: self)
            return
        }
        
        
        guard let phone = phoneTF.text, phone != ""
        else {
            GlobalMethod.init().showAlert(title: "Phone Number", message: "Please enter your phone number!", vc: self)
            return
        }
        
        guard let dob = DOBTF.text, dob != ""
        else {
            GlobalMethod.init().showAlert(title: "Date of birth", message: "Please enter your date of birth!", vc: self)
            return
        }
        
        
        if !(e.isValidEmail) {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Your email address is incorrect!", vc: self)
        } else if phone.count < 10 {
            GlobalMethod.init().showAlert(title: "Phone", message: "Phone number should be at least 10 digits!", vc: self)
        }
        else if DOBTF.text!.count > 1 {
            let age = Calendar.current.dateComponents([.year], from: datePickerView.date, to: Date()).year!
            print(age)  // 16
            if age < 18 {
                GlobalMethod.init().showAlert(title: "Date of birth", message: "You must be older than 18 years!", vc: self)
            }
            else {
                let df = DateFormatter()
                df.dateFormat = "yyyy-MM-dd"
                let dateStr = df.string(from: datePickerView.date)
                updateProfile(name: name, email: e, location: address, DOB: dateStr, phone: phone, isLoading: true)

            }
        }
        else {
            updateProfile(name: name, email: e, location: address, DOB: dob, phone: phone, isLoading: true)
            
        }
    }
    
    @IBAction func updateLocationAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

    }
}


extension EditProfileVC {
    
    //Fetch Popular
    private func updateProfile(name:String, email:String, location:String, DOB: String,phone: String, isLoading: Bool) -> Void {
    
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        print(selectedImg)
        
        if let data = selectedImg?.jpegData(compressionQuality: 0.5) {
            let parameters = [
                "session_id": session,
                "user_name": name,
                "email": email,
                "address": location,
                "latitude": lat,
                "longitude": long,
                "phone": phone,
                "dob":DOB,
                "device_type": "2",
                "device_id": "12345678",
                "device_token": UserDefaults.standard.value(forKey: "FCM_TOKEN") ?? "12345",
            ]
            updateProfileInfo(image: data, parameters: parameters)
        } else {
            GlobalMethod.init().showAlert(title: "Food Image", message: "Please select product image!", vc: self)
        }
        
        
//        Loader.shared.show()
//
//        AF.request(EDIT_PROFILE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
//            switch response.result {
//            case .success (let res):
//                self.exterateData(rss: JSON(res))
//            case .failure(let error):
//                print(error.localizedDescription)
//                DispatchQueue.main.async(execute: {
//                    Loader.shared.hide()
//                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
//                })
//            }
//        }
    }

    
    func updateProfileInfo(image:Data?, parameters: [String : Any]) {
        
        Loader.shared.show()
        
        AF.upload(multipartFormData: { (MultipartFormData) in
            for (key, value) in parameters {
                MultipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            if image != nil {
                MultipartFormData.append(image!, withName: "profile_image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
        },to: EDIT_PROFILE, usingThreshold: UInt64.init(),
          method: .post).response { response in
            
            print(response)
            
            switch response.result {
            case .success (let res):
                guard  let r = res else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: "Please try again latter!", vc: self)
                    })
                    return
                }
                
                self.exterateData(rss: JSON(r))
                
//                if JSON(r)["status"].intValue == 200 {
//                    DispatchQueue.main.async(execute: {
//                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(r)["message"].stringValue, vc: self)
//                    })
//                } else {
//                    GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(r)["message"].stringValue, vc: self)
//                }
//                self.navigationController?.popViewController(animated: true)

                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
            
            DispatchQueue.main.async(execute: {
                Loader.shared.hide()
            })
        }
    }


    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce => ", rss)
        
        if rss["status"].intValue == 200  {
            guard let userInfo = rss["profile"].dictionary
                else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                    })
                    return
            }
            UserDefaults.standard.setValue(userInfo["user_name"]?.stringValue, forKey: "user_name")
            UserDefaults.standard.setValue(userInfo["email"]?.stringValue, forKey: "email")

            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
            
        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }

    
    
}


// MARK: - Google Place Picker

extension EditProfileVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.locTF.text = String(place.name ?? "")
        address = locTF.text ?? ""
        lat = String(place.coordinate.latitude)
        long = String(place.coordinate.longitude)
        getAddressFromLatLon(address: place.name ?? "", pdblLatitude: String(place.coordinate.latitude), withLongitude: String(place.coordinate.longitude))

        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func getAddressFromLatLon(address: String, pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        print(lat)
        print(lon)
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                print(placemarks)
                
                if let pm = placemarks {
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        print(pm.country)
                        
                        print(pm.locality)
                        
                        print(pm.subLocality)
                        
                        print(pm.thoroughfare)
                        
                        print("zip code", pm.postalCode)
                        
                        print(pm.subThoroughfare)
                        
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        print(addressString)
                    }
                }
        })
        
    }
    
    
}

extension EditProfileVC: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func selectImage() {
        let alertController = UIAlertController(title: "Perform Action", message: nil, preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: "Choose From Library", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let camera = UIAlertAction(title: "Take From Camera", style: .default, handler: { (action) -> Void in
            
            if !UIImagePickerController.isSourceTypeAvailable(.camera){
                
                let alertController = UIAlertController.init(title: "Alert", message: "Device has no camera!", preferredStyle: .alert)
                
                let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
                })
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                self.self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(library)
//        alertController.addAction(camera)
        alertController.addAction(cancel)
        
        self.navigationController?.present(alertController, animated: true){}
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print(image)
            self.prodImg.image = image
            selectedImg = self.prodImg.image
            
//            profileImg.image = image
//
            if let data = image.jpegData(compressionQuality: 0.5), let session = UserDefaults.standard.value(forKey: "session") as? String {
                 // self.uploadImage(image: data, parameters: ["session_id": session])
            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
}
