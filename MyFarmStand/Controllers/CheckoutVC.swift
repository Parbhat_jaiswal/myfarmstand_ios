//
//  CheckoutVC.swift
//  AyMaEat
//
//  Created by Prabhat on 26/04/20.
//  Copyright © 2020 Lee Da Hang Pte Ltd. All rights reserved.
//

import UIKit

import Alamofire
import SwiftyJSON

import Stripe

protocol CheckoutProtocal {
    func dissmissVC()
}

class CheckoutVC: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var totalAmount: UILabel!
    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var number: MaxLengthTextField!
    @IBOutlet weak var Expiry: UITextField!
    @IBOutlet weak var ExpiryYY: UITextField!

    @IBOutlet weak var cvc: MaxLengthTextField!
    
    var delegate: CheckoutProtocal?
    
    var price: Double = 0.0
    var totalPrice: Double = 0.0
    fileprivate var _popUpVC = UIViewController()
    
    var cartId: String?
    
    let month = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"]
    let year = ["21", "22", "23", "24", "25", "26", "27", "28", "29", "30"]
    var m: String = ""
    var y: String = ""

    let thePicker = UIPickerView()
    let thePicker1 = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewResizerOnKeyboardShown()

//        name.text = "Prabhat jaiswal"
//        number.text = "4111 1111 1111 1111"
//        Expiry.text = "12"
//        ExpiryYY.text = "25"
//        cvc.text = "123"
        
        totalPrice = price
        
        itemPrice.text = "$" + "\(totalPrice)"
        totalAmount.text = "$" + "\(totalPrice)"
        
        Expiry.delegate = self
        ExpiryYY.delegate = self
        
        thePicker.delegate = self
        Expiry.inputView = thePicker
        
        thePicker1.delegate = self
        ExpiryYY.inputView = thePicker1

    }
    
    @IBAction func crossBtnAction(_ sender: UIButton) {
        delegate?.dissmissVC()
    }
    
    @IBAction func processToPay(_ sender: UIButton) {
        
        guard let n = name.text, n != ""
        else {
            GlobalMethod.init().showAlert(title: APP_NAME, message: "Please enter your full name!", vc: self)
            return
        }
        
        guard let num = number.text, num != ""
        else {
            GlobalMethod.init().showAlert(title: APP_NAME, message: "Please enter your card number!", vc: self)
            return
        }
        
        guard let exp = Expiry.text, exp != ""
        else {
            GlobalMethod.init().showAlert(title: APP_NAME, message: "Please enter expiry month is required!", vc: self)
            return
        }
        
        guard let yy = ExpiryYY.text, yy != ""
        else {
            GlobalMethod.init().showAlert(title: APP_NAME, message: "Please enter expiry year is required!", vc: self)
            return
        }
        
        
        guard let cvcNum = cvc.text, cvcNum != ""
        else {
            GlobalMethod.init().showAlert(title: APP_NAME, message: "Please enter cvc number of your card!", vc: self)
            return
        }
        
        
        Loader.shared.show()
        
        let f = UInt(exp)
        let l = UInt(yy)
        
        let cardParams = STPCardParams()
        cardParams.name = n
        cardParams.number = num
        cardParams.expMonth = f!
        cardParams.expYear =  l!
        cardParams.cvc = cvcNum
        
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            print("Printing Strip response:\(String(describing: token?.allResponseFields))\n\n")
            print("Printing Strip Token:\(String(describing: token?.tokenId))")
            
            if error != nil {
                GlobalMethod.init().showAlert(title: APP_NAME, message: error?.localizedDescription ?? "Please check your card information.", vc: self)
                print(error?.localizedDescription ?? "")
                Loader.shared.hide()
            }

            if token != nil {
                // self.msgBox.text = "Transaction success! \n\nHere is the Token: \(String(describing: token!.tokenId))\nCard Type: \(String(describing: token!.card!.funding))\n\nSend this token or detail to your backend server to complete this payment."
                
                if let cId = self.cartId {
                     self.orderProcess(transactionId: token!.tokenId, amount: self.totalAmount.text!, cart_ids: cId)
                }
                
            }
        }
    }
    
}

extension CheckoutVC {
    func orderProcess(transactionId: String, amount: String, cart_ids: String) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        guard let userId = UserDefaults.standard.value(forKey: "User_Id") as? String else { return }
        
        let params = [
            "session_id":token,
            "user_id":userId,
            "transaction_id":transactionId,
            "amount": totalPrice,
            "cart_ids": cart_ids
        ] as [String : Any]
        
        print(params)
        
        AF.request(ORDER_PROCESS, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            
            print(response)
            
            switch response.result {
            case .success (let res):
                
                print("Server Responce :-> ", JSON(res))
                
                let JSONInfo = JSON(res)
                
                if JSONInfo["message"].stringValue == "Payment received." {
                    DispatchQueue.main.async {
                        self.showThankYou()
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSONInfo["message"].string ?? "Please try again latter!", vc: self)
                    })
                }
                
                if JSONInfo["message"].stringValue == "Invalid Token" {
                    GlobalMethod.init().removeKey()
                }
                
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}

extension CheckoutVC: ThankYouVCProtocal {
    
    func showThankYou() {
        if !(_popUpVC.isViewLoaded){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loadVC = storyboard.instantiateViewController(withIdentifier: "ThankYouVC_Id") as! ThankYouVC
            loadVC.delegate = self
            self.addChild(loadVC)
            self.view.addSubview(loadVC.view)
            loadVC.view.alpha = 0
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                loadVC.view.alpha = 1
            }, completion: nil)
            
            _popUpVC = loadVC
        }
    }
    
    func dissmiss() {
        if (_popUpVC.isViewLoaded) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self._popUpVC.view.alpha = 0
            }, completion: nil)
            _popUpVC.view.removeFromSuperview()
            _popUpVC = UIViewController()
        }
    }
    
    func thankYouDissmissByOk() {
        if (_popUpVC.isViewLoaded) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self._popUpVC.view.alpha = 0
            }, completion: nil)
            _popUpVC.view.removeFromSuperview()
            _popUpVC = UIViewController()
            
            delegate?.dissmissVC()
        }
    }
    
}

extension CheckoutVC: UIPickerViewDelegate, UIPickerViewDataSource{
    // MARK: UIPickerView Delegation
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == thePicker {
            return month.count
        } else {
            return year.count
        }
    }

    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == thePicker {
            return month[row]
        } else {
            return year[row]
        }
    }

    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == thePicker {
            Expiry.text = month[row]
        } else{
            ExpiryYY.text = year[row]
        }
    }

}


extension CheckoutVC {
    
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowForResizing), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideForResizing), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let window = self.view.window?.frame {
            // We're not just minusing the kb height from the view height because
            // the view could already have been resized for the keyboard before
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: window.origin.y + window.height - keyboardSize.height)
        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
    }
    
}
