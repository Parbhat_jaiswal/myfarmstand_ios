//
//  ThankYouVC.swift
//  AyMaEat
//
//  Created by Prabhat on 06/05/20.
//  Copyright © 2020 Lee Da Hang Pte Ltd. All rights reserved.
//

import UIKit


protocol ThankYouVCProtocal {
    func dissmiss()
    func thankYouDissmissByOk()
}

class ThankYouVC: UIViewController {
    
    var delegate:ThankYouVCProtocal?
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func okAction(_ sender: UIButton) {
        delegate?.thankYouDissmissByOk()
    }
    
}
