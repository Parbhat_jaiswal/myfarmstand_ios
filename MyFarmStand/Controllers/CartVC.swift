//
//  CartVC.swift
//  AyMaEat
//
//  Created by Prabhat on 23/03/20.
//  Copyright © 2020 Lee Da Hang Pte Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CartVC: UIViewController {
    
    @IBOutlet weak var cartTblView: UITableView!
    @IBOutlet weak var emptyLbl: UILabel!
    @IBOutlet weak var checkOutView: DesignableView!
    
    var cartListArr:[CartModel] = []
    
    var isComeFrom: String?
    
    fileprivate var _popUpVC = UIViewController()

    var totalPrice: Double = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String  {
            getCart(userId: userId)
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
        
    @IBAction func checkOutAction(_ sender: UIButton) {
        
//        let storyboard = UIStoryboard(name: "User", bundle: nil)
//        let loadVC = storyboard.instantiateViewController(withIdentifier: "CheckoutVC_Id") as! CheckoutVC
//        GlobalMethod.init().pushToVC(storyBoard: "User", VCId: "CheckoutVC_Id", VC: self)
         
        if !(_popUpVC.isViewLoaded) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loadVC = storyboard.instantiateViewController(withIdentifier: "CheckoutVC_Id") as! CheckoutVC
            loadVC.delegate = self
            loadVC.price = self.totalPrice

            if self.cartListArr.count != 0 {
                let id = NSMutableArray()
                for i in cartListArr {
                    id.add(i.cart_id!)
                }
                loadVC.cartId = id.componentsJoined(by: ",")
            }
            
            self.addChild(loadVC)
            self.view.addSubview(loadVC.view)
            loadVC.view.alpha = 0

            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                loadVC.view.alpha = 1
            }, completion: nil)

            _popUpVC = loadVC
        }
        
    }
    
}

extension CartVC: CheckoutProtocal{
    func dissmissVC() {
        if (_popUpVC.isViewLoaded) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self._popUpVC.view.alpha = 0
            }, completion: nil)
            _popUpVC.view.removeFromSuperview()
            _popUpVC = UIViewController()
            
            if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String  {
                getCart(userId: userId)
            }
        }
    }
}

extension CartVC: UITableViewDataSource & UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if cartListArr.count == 0 {
            emptyLbl.isHidden = false
            checkOutView.isHidden = true
        } else {
            emptyLbl.isHidden = true
             checkOutView.isHidden = false
        }
        return cartListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartCell_Id", for: indexPath) as! CartCell
        let index = cartListArr[indexPath.row]
        cell.setupInfo(info: index)
        
        cell.removeBtn.tag = indexPath.row
        cell.removeBtn.addTarget(self, action: #selector(removeFoodFromcartAction), for: .touchUpInside)

        cell.plusBtn.tag = indexPath.row
        cell.plusBtn.addTarget(self, action: #selector(plusBtnAction), for: .touchUpInside)

        cell.minusBtn.tag = indexPath.row
        cell.minusBtn.addTarget(self, action: #selector(minusBtnAction), for: .touchUpInside)

        return cell
    }
    
    @objc func removeFoodFromcartAction(_ sender: UIButton) {
        let dialogMessage = UIAlertController(title: APP_NAME, message: "Are you sure you want to delete this food from the cart?", preferredStyle: .alert)
        // Create OK button with action handler
        let ok = UIAlertAction(title: "YES", style: .destructive, handler: { (action) -> Void in
            if let Id = self.cartListArr[sender.tag].cart_id, let n = self.cartListArr[sender.tag].productName?.capitalized {
                self.removeFood(id: "\(Id)", name: n)
            }
        })
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "NO", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @objc func plusBtnAction(_ sender: UIButton) {
        let index = cartListArr[sender.tag]
        
        var q = Int()
        if let qty = index.quantity {
            q = qty + 1
        }
        
        if let p = index.actualPrice {
            
            let d = Double(p)
            let qt = Double(q)

            let total_price =  qt * d!
            let formatted = String(format: "%.2f", total_price)
            
            plusMinus(id: index.cart_id!, qty: "\(q)", actualPrice: index.actualPrice!, totalPrice: formatted)
        }
    }
    
    @objc func minusBtnAction(_ sender: UIButton) {
        let index = cartListArr[sender.tag]
        var q = Int()
        if let qty = index.quantity {
            if qty > 1 {
                q = qty - 1
            }
            
            if let p = index.actualPrice {
                
                let d = Double(p)
                let qt = Double(q)

                let total_price =  qt * d!
                let formatted = String(format: "%.2f", total_price)
                
                plusMinus(id: index.cart_id!, qty: "\(q)", actualPrice: index.actualPrice!, totalPrice: formatted)
            }
            
        }
        
    }
    
}


extension CartVC  {
    
    func getCart(userId: String) -> Void {
        
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }

        let params = [
            "session_id":token,
            "user_id": userId
        ] as [String : Any]
                
        
        Loader.shared.show()
        AF.request(SHOW_CART, method:.post, parameters: params, encoding: JSONEncoding.default).responseJSON { response in
            
            print("Server Responce :-> ", response)
            
            switch response.result {
            case .success (let res):
                
                print("Server Responce :-> ", JSON(res))
                
                let JSONInfo = JSON(res)
                
                if JSONInfo["message"].stringValue == "Cart Information." {
                    self.cartListArr.removeAll()
                    
                    for i in JSONInfo["cart_details"].arrayValue {
                        self.cartListArr.append(CartModel.init(info: i))
                    }
                    
                    self.totalPrice = 0.0
                    
                    for j in self.cartListArr {
                        if let p = j.totalPrice {
                            if let pp = Double(p) {
                                self.totalPrice = self.totalPrice + pp
                            }
                        }
                    }
                    
                                        
                }else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSONInfo["message"].string ?? "Please try again latter!", vc: self)
                    })
                }
                
                if JSONInfo["message"].stringValue == "Invalid Token" {
                    GlobalMethod.init().removeKey()
                }
                
                DispatchQueue.main.async(execute: {
                    self.cartTblView.reloadData()
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    
    func removeFood(id: String, name: String) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }

        let params = [
            "session_id":token,
            "cart_id": id
        ] as [String : Any]

        Loader.shared.show()
        AF.request(REMOVE_CART, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
                
            case .success (let res):
                
                print("Server Responce :-> ", JSON(res))
                
                let JSONInfo = JSON(res)
                
                if JSONInfo["message"].stringValue == "Product Removed From Cart Successfully." {
                    if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String  {
                        self.getCart(userId: userId)
                    }
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: "\(name) has been removed successfully from the cart!", vc: self)
                    })
                }else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSONInfo["message"].string ?? "Please try again latter!", vc: self)
                    })
                }
                
                if JSONInfo["message"].stringValue == "Invalid Token" {
                    GlobalMethod.init().removeKey()
                }
                
                DispatchQueue.main.async(execute: {
                    self.cartTblView.reloadData()
                    Loader.shared.hide()
                })

                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func plusMinus(id: Int, qty: String, actualPrice: String, totalPrice: String) -> Void {
        
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }

        let params = [
            "session_id":token,
            "cart_id":id,
            "product_actual_price":actualPrice,
            "product_quantity":qty,
            "product_total_price":totalPrice
        ] as [String : Any]
        
        Loader.shared.show()
        AF.request(ADD_CART_PLUSE_MINUSE, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
                
            case .success (let res):
                
                print("Server Responce :-> ", JSON(res))
                
                let JSONInfo = JSON(res)
                
                if JSONInfo["message"].stringValue == "Product Information updated in cart successfully." {
                    if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String  {
                        self.getCart(userId: userId)
                    }
                }else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSONInfo["message"].string ?? "Please try again latter!", vc: self)
                    })
                }
                
                if JSONInfo["message"].stringValue == "Invalid Token" {
                    GlobalMethod.init().removeKey()
                }
                
                DispatchQueue.main.async(execute: {
                    self.cartTblView.reloadData()
                    Loader.shared.hide()
                })

                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
}




class CartCell: UITableViewCell {
    
    
    @IBOutlet weak var foodName: UILabel!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupInfo(info: CartModel) {
        foodName.text = info.productName?.capitalized
        if let price = info.totalPrice {
            totalPrice.text = "$" + price
        }
        
        if let qty = info.quantity {
            quantity.text = "\(qty)"
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
