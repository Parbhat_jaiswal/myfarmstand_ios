//
//  MyProductVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 23/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class MyProductVC: UIViewController {

    @IBOutlet weak var prodTable: UITableView!
    @IBOutlet weak var noLbl: UILabel!
    
    var prodList = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        prodTable.delegate = self
        prodTable.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getProduct(isLoading: true)
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension MyProductVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return prodList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyProductCell
        let index = JSON(prodList[indexPath.row])
                
        let imgData = index["images"].arrayValue
        if imgData.count != 0 {
            let img = imgData[0].dictionaryValue
            let imgName = img["image"]?.stringValue ?? ""
            let imgUrl = index["images_base_path"].stringValue + imgName
            if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                cell.prodImg?.kf.indicatorType = .activity
                cell.prodImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
            }
        }
        
        cell.prodLoc.text = index["title"].stringValue
        cell.prodPriceLbl.text = (index["currency"].stringValue + index["price"].stringValue)
        cell.prodDescLbl.text = index["description"].stringValue
        cell.prodDateLbl.text = index["created_at"].stringValue
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction1(_:)), for: .touchUpInside)

        return cell
    }
    
    @objc func deleteBtnAction1(_ sender: UIButton) {
        let index = JSON(prodList[sender.tag])
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to delete?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (alert: UIAlertAction!) -> Void in
            self.deleteById(id: index["product_id"].intValue, index: sender.tag)
        }
        let no = UIAlertAction(title: "No", style: .default) { (alert: UIAlertAction!) -> Void in
            
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)
    }

}

extension MyProductVC {
    
    private func deleteById(id: Int, index: Int) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        guard let userId = UserDefaults.standard.value(forKey: "User_Id") as? String else { return }

        let parameters = [
            "session_id":token,
            "user_id": userId,
            "product_id":id
        ]
        as [String : Any]
        
        print(parameters)
        
        Loader.shared.show()
        
        AF.request(DELETE_PRODUCTS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(JSON(res))
                
                if JSON(res)["status"].intValue == 200 {
                    DispatchQueue.main.async(execute: {
                        self.prodList.removeObject(at: index)
                        self.prodTable.reloadData()
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["error_description"].stringValue, vc: self)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["error_description"].stringValue, vc: self)
                    })

                }
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}


extension MyProductVC {
    
    //Add to Favt
    func getProduct(isLoading: Bool)  {
        
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { print("no session id found in search API")
            return}
        let parameters = [
            "session_id": session
        ]
        
        Loader.shared.show()
        
        AF.request(MY_PRODUCE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")
        
        if rss["status"].intValue == 200 {
            prodList.removeAllObjects()
            for i in rss["data"].arrayValue {
                prodList.add(i)
            }
            
            DispatchQueue.main.async(execute: {
                self.prodTable.reloadData()
                self.prodTable.isHidden = self.prodList.count == 0
                self.noLbl.isHidden = !(self.prodList.count == 0)
            })
            
        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
}

//MARK: - Custom Tableview cell
class MyProductCell : UITableViewCell {
    @IBOutlet weak var prodDescLbl: UILabel!
    
    @IBOutlet weak var prodImg: UIImageView!
    @IBOutlet weak var prodLoc: UILabel!
    @IBOutlet weak var prodPriceLbl: UILabel!
    @IBOutlet weak var prodDateLbl: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
}



