//
//  NotificationVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 22/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class NotificationVC: UIViewController {

    @IBOutlet weak var notificationTable: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    var notificationArr = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.fetchNotificationList(isLoading: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.fetchNotificationList(isLoading: true)
    }
}



extension NotificationVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell_Id", for: indexPath) as! NotificationCell
        let index = JSON(notificationArr[indexPath.row])
        cell.dateLbl.text = index["created_date"].stringValue
        cell.descriptionLbl.text = index["message"].stringValue.capitalized
        
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(_:)), for: .touchUpInside)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    @objc func deleteBtnAction(_ sender: UIButton) {
        let dialogMessage = UIAlertController(title: APP_NAME, message: "Are you sure you want to delete?", preferredStyle: .alert)
        // Create OK button with action handler
        let ok = UIAlertAction(title: "YES", style: .destructive, handler: { (action) -> Void in
            let index = JSON(self.notificationArr[sender.tag])
            self.deleteByApi(Nid: index["notification_id"].stringValue)
        })
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "NO", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        //Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        // Present dialog message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
}


extension NotificationVC {
    //fetch Favt
    private func fetchNotificationList(isLoading: Bool) -> Void {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        let id = UserDefaults.standard.string(forKey: "User_Id") ?? ""
        let parameters = [
            "user_id":id,
            "session_id": session,
        ]
        print("SESSION =>>",session)
        Loader.shared.show()
        
        AF.request(NOTIFICATIONS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")

        if rss["status"].intValue == 200  {
            notificationArr.removeAllObjects()
            for i in rss["notification_details"].arrayValue {
                notificationArr.add(i)
            }
            
            DispatchQueue.main.async(execute: {
                self.notificationTable.reloadData()
                self.notificationTable.isHidden = self.notificationArr.count == 0
                self.noDataLbl.isHidden = !(self.notificationArr.count == 0)
            })

        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
    
    private func deleteByApi(Nid: String) -> Void {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""
        let id = UserDefaults.standard.string(forKey: "User_Id") ?? ""
        let parameters = [
            "user_id":id,
            "session_id": session,
            "notification_id":Nid
        ]
        print("SESSION =>>",session)
        Loader.shared.show()
        
        AF.request(DELETE_NOTIFICATIONS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(JSON(res))
                Loader.shared.hide()
                self.fetchNotificationList(isLoading: false)
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
}


class NotificationCell: UITableViewCell {
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    var deleteClosure : ((NotificationCell) -> Void)?
    @IBAction func deleteAction(_ sender: UIButton) {
        deleteClosure?(self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}




