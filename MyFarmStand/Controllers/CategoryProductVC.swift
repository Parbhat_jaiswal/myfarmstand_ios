//
//  CategoryProductVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 22/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class CategoryProductVC: UIViewController {
    
    @IBOutlet weak var search: UISearchBar!
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var productCollectionView: UICollectionView!
    
    @IBOutlet weak var searchBarTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noDataLbl: UILabel!
    
    var categoryList = NSMutableArray()
    var productByCat = [ProductByCatModel]()
    var selectIndex: Int?
    
    fileprivate var popUpView = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getCategory(isLoading: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    @IBAction func searchBtnAction(_ sender: Any) {
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            searchBarTopConstraint.constant = 0
            if let cancelButton = search.value(forKey: "cancelButton") as? UIButton{
                cancelButton.isEnabled = true
            }
            search.becomeFirstResponder()
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()
            })
        } else {
            self.closeView()
        }
    }
    
    @objc func closeSearch() {
        search.resignFirstResponder()
        search.text = ""
        
        searchBarTopConstraint.constant = -(self.search.frame.height)
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
    }
}


//MARK: - Declaration of CollectionView Delegate Methods

extension CategoryProductVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == categoryCollection  {
            return categoryList.count
            
        } else {
            return productByCat.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == productCollectionView {
            let FavProductCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavProductCell_Id", for: indexPath) as! FavProductCell
            
            let index = productByCat[indexPath.row]
            FavProductCell.setupCell(index: index)
            
            FavProductCell.favtBtn.tag = indexPath.row
            FavProductCell.favtBtn.addTarget(self, action: #selector(heartButtonPressed(_:)), for: .touchUpInside)
            
            FavProductCell.addBtn.tag = indexPath.row
            FavProductCell.addBtn.addTarget(self, action: #selector(addBtnAction(_:)), for: .touchUpInside)

            
            FavProductCell.deleteBtn.tag = indexPath.row
            FavProductCell.deleteBtn.addTarget(self, action: #selector(deleteBtnAction(_:)), for: .touchUpInside)

            if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
                if let userId = UserDefaults.standard.value(forKey: "User_Id") as? String {
                    if index.posted_by_id == Int(userId) {
                        FavProductCell.deleteBtn.isHidden = false
                        FavProductCell.addBtnView.isHidden = true
                    } else {
                        FavProductCell.deleteBtn.isHidden = true
                        FavProductCell.addBtnView.isHidden = false
                    }
                }
            } else {
                FavProductCell.deleteBtn.isHidden = true
            }
            
            return FavProductCell
            
        } else {
            let CategoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell_Id", for: indexPath) as! CategoryCell
            
            let index = JSON(categoryList[indexPath.row])
            CategoryCell.catName.text = index["category_name"].stringValue.capitalized
            
            if selectIndex == indexPath.row {
                CategoryCell.catView.backgroundColor = #colorLiteral(red: 0.2710425556, green: 0.6824354529, blue: 0.934972465, alpha: 1)
                CategoryCell.catName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                
            } else {
                CategoryCell.catView.backgroundColor = #colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1)
                CategoryCell.catName.textColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
            
            return CategoryCell
        }
        
    }
    
    @objc func deleteBtnAction(_ sender: UIButton) {
        let index = productByCat[sender.tag]
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to delete?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (alert: UIAlertAction!) -> Void in
            self.deleteById(id: index.product_id ?? 0, index: sender.tag)
        }
        let no = UIAlertAction(title: "No", style: .default) { (alert: UIAlertAction!) -> Void in
            
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)
    }
    
    @objc func addBtnAction(_ sender: UIButton) {
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            let index = productByCat[sender.tag]
             addToCartByApi(user_id: index.posted_by_id!, product_id: index.product_id!, product_actual_price: index.price!, foodName: index.title!)
        } else {
            self.closeView()
        }
    }
    
    @objc func heartButtonPressed(_ sender: UIButton) {
        if let _ = UserDefaults.standard.value(forKey: "session_id") as? String  {
            let index = productByCat[sender.tag]
            addRemoveFromFav(id: "\(index.product_id!)", favouriteStoreIndex:sender.tag)
        } else {
            self.closeView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == productCollectionView {
            return CGSize(width: self.view.frame.size.width  / 2 - 20, height: 250)
        } else {
            return CGSize(width: 206, height: 49)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == productCollectionView {
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "ProductDetailVC_Id") as! ProductDetailVC
            vc.productByCat = productByCat[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
            
        } else {
            let index = JSON(categoryList[indexPath.row])
            productByCat = []
            self.fetchProductList(isLoading: true, productId:  index["id"].stringValue)
            selectIndex = indexPath.row
            categoryCollection.reloadData()
        }
        
    }
    
}

extension CategoryProductVC: AuthenticationViewProtocal {
    
    func closeView() {
        if !(popUpView.isViewLoaded) {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if let loadVC = storyboard.instantiateViewController(withIdentifier: "AuthenticationPopUpID") as? AuthenticationPopUp {
                loadVC.delegate = self
                self.addChild(loadVC)
                self.view.addSubview(loadVC.view)
                popUpView.view.alpha = 0
                UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                    self.popUpView.view.alpha = 1
                }, completion: nil)
                popUpView = loadVC
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self.popUpView.view.alpha = 0
            }, completion: nil)
            popUpView.view.removeFromSuperview()
            popUpView = UIViewController()
        }
        
    }
    
}

extension CategoryProductVC {
    
    func getCategory(isLoading: Bool) -> Void {
        
        if isLoading == true { Loader.shared.show() }
        
        AF.request(CATEGORY, method:.get, encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success (let res):
                
                if JSON(res)["status"].intValue == 200 {
                    let JSONInfo = JSON(res)["data"].arrayValue
                    self.selectIndex = 0
                    self.categoryList.removeAllObjects()
                    for i in JSONInfo {
                        self.categoryList.add(i)
                    }
                    
                    if self.categoryList.count != 0 {
                        self.fetchProductList(isLoading: false, productId:  JSON(self.categoryList[0])["id"].stringValue)
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.categoryCollection.reloadData()
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: "Please try again latter!", vc: self)
                    })
                }
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func fetchProductList(isLoading: Bool, productId: String) -> Void {
        var parameters = Parameters()
        
        if let session = UserDefaults.standard.string(forKey: "session_id") {
            parameters = [
                "session_id": session,
                "category": productId
            ]
        } else {
            parameters = [
                "category": productId
            ]
        }
        
        Loader.shared.show()
        
        AF.request(PRODUCT_BY_CATEGORY, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    //Add to Favt
    func addRemoveFromFav(id: String, favouriteStoreIndex:Int) {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        let parameters = [
            "session_id": session,
            "product_id" : id
        ]
        
        Loader.shared.show()
        
        AF.request(ADD_REMOVE_FAVORITE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):

                if  JSON(res)["message"].stringValue != "Removed from Favorites" {

                    // Favourite
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Store favourite successfully!", vc: self)

                    let indexPath = IndexPath(row:favouriteStoreIndex, section: 0)
                    let cell = self.productCollectionView.cellForItem(at: indexPath) as! FavProductCell
                    cell.favtBtn.setImage(UIImage(named: "red_heart"), for: .normal)

                } else {
                    // Unfavourite
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Store unfavourite successfully!", vc: self)

                    let indexPath = IndexPath(row: favouriteStoreIndex, section: 0)
                    let cell = self.productCollectionView.cellForItem(at: indexPath) as! FavProductCell

                    cell.favtBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
                }
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")
        
        if rss["message"].stringValue == "Products By Category Fetched"  {
            
            productByCat.removeAll()
            for i in rss["data"].arrayValue {
                productByCat.append(ProductByCatModel.init(info: JSON(i)))
            }
            
            DispatchQueue.main.async(execute: {
                self.productCollectionView.reloadData()
                self.productCollectionView.isHidden = self.productByCat.count == 0
                self.noDataLbl.isHidden = !(self.productByCat.count == 0)
            })
            
        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
    //MARK: - Search API
    func searchProduct(isLoading: Bool, str: String) -> Void {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { print("no session id found in search API")
            return}
        let parameters = [
            "session_id": session,
            "search" : str
        ]
        
        Loader.shared.show()
        
        AF.request(SEARCH_PRODUCTS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.extractSearchData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    func extractSearchData(rss: JSON) -> Void {
        print("Server Responce  =>  \(rss)")
        
        if rss["status"].intValue == 200  {
            productByCat.removeAll()
            for i in rss["data"].arrayValue {
                productByCat.append(ProductByCatModel.init(info: JSON(i)))
            }
        }
        
        DispatchQueue.main.async(execute: {
            self.productCollectionView.reloadData()
            self.productCollectionView.isHidden = self.productByCat.count == 0
            self.noDataLbl.isHidden = !(self.productByCat.count == 0)
        })
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
        
        if rss["error_description"].stringValue == "Your login session has been expired." {
            GlobalMethod.init().removeKey()
            
            let alert = UIAlertController(title: "Success", message: "You have been successfully logged out!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
                let controllers : Array = self.navigationController!.viewControllers
                self.navigationController!.popToViewController(controllers[0], animated: true)
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion:nil)

        }
    }
    
    private func deleteById(id: Int, index: Int) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        guard let userId = UserDefaults.standard.value(forKey: "User_Id") as? String else { return }

        let parameters = [
            "session_id":token,
            "user_id": userId,
            "product_id":id
        ]
        as [String : Any]
        
        print(parameters)
        
        Loader.shared.show()
        
        AF.request(DELETE_PRODUCTS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(JSON(res))
                
                if JSON(res)["status"].intValue == 200 {
                    DispatchQueue.main.async(execute: {
                        self.productByCat.remove(at: index)
                        self.productCollectionView.reloadData()
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["error_description"].stringValue, vc: self)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(res)["error_description"].stringValue, vc: self)
                    })

                }
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    
}


extension CategoryProductVC {
    
    func addToCartByApi(user_id: Int, product_id: Int, product_actual_price: String, foodName: String) -> Void {
        
        guard let session = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        
        let params = [
            "session_id":session,
            "user_id":user_id,
            "product_id":product_id,
            "product_actual_price":product_actual_price,
            "product_quantity":"1",
            "product_total_price":product_actual_price,
            "is_show":"1"
        ] as [String : Any]
        
        print(params)
        
        Loader.shared.show()
        //        AF.request(ADD_CART, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
        
        print(ADD_CART)
        print(params)
        
        AF.request(ADD_CART, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            print("Server Responce :-> ", response)
            
            switch response.result {
            case .success (let res):
                
                print("Server Responce :-> ", JSON(res))
                
                let JSONInfo = JSON(res)
                
                if JSONInfo["message"].stringValue == "Cart updated." {
                    
                    let myalert = UIAlertController(title: APP_NAME, message: "\(foodName.capitalized) successfully added in your cart!", preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        
                        //                        let story =  UIStoryboard.init(name: "User", bundle: nil)
                        //                        let vc = story.instantiateViewController(withIdentifier: "CartVC_Id") as! CartVC
                        //                        vc.isComeFrom = "FoodDetails"
                        //                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    })
                    self.present(myalert, animated: true)
                    
                } else if JSONInfo["error_description"].stringValue == "Product added in cart successfully." {
                    
                    let myalert = UIAlertController(title: APP_NAME, message: "\(foodName.capitalized) successfully added in your cart!", preferredStyle: UIAlertController.Style.alert)
                    myalert.addAction(UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                        
                        //                        let story =  UIStoryboard.init(name: "User", bundle: nil)
                        //                        let vc = story.instantiateViewController(withIdentifier: "CartVC_Id") as! CartVC
                        //                        vc.isComeFrom = "FoodDetails"
                        //                        self.navigationController?.pushViewController(vc, animated: true)
                        
                    })
                    self.present(myalert, animated: true)
                    
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: JSONInfo["message"].string ?? "Please try again latter!", vc: self)
                    })
                }
                
                
                
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}


//MARK: - Declaration of Searchbar Delegate Methods
extension CategoryProductVC : UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        getCategory(isLoading: true)
        self.closeSearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count > 0 {
            self.searchProduct(isLoading: false, str: searchText)
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}

//MARK: - Declaration of UICollection View Cell

class FavProductCell: UICollectionViewCell {
    @IBOutlet weak var addBtn: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    @IBOutlet weak var favtBtn: UIButton!
    @IBOutlet weak var productDescLbl: UILabel!
    
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var addBtnView: DesignableView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(index: ProductByCatModel) {
        if index.images.count != 0 {
            let imgUrl = index.images_base_path! + index.images[0].image!
            if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                productImg?.kf.indicatorType = .activity
                productImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "image"), options: [.transition(.fade(0.7))], progressBlock: nil)
            }
        }
        productNameLbl.text = index.title?.capitalized
        productPriceLbl.text = index.currency! + index.price!
        productDescLbl.text = index.desc?.capitalized
        if index.is_favourite == 1 {
            favtBtn.setImage(UIImage(named: "red_heart"), for: .normal)
        } else {
            favtBtn.setImage(UIImage(named: "gray_heart"), for: .normal)
        }
        
//        index
        
    }

}

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var catView: DesignableView!
    @IBOutlet weak var catName: UILabel!
    
    
}


