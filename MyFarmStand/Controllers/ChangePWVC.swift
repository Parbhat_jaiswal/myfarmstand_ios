//
//  ChangePWVC.swift
//  MyFarmStand
//
//  Created by Ankit Ahluwalia on 14/11/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class ChangePWVC: UIViewController {

    @IBOutlet weak var oldPWTF: UITextField!
    @IBOutlet weak var newPWTF: UITextField!
    @IBOutlet weak var confirmPWTF: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeAction(_ sender: Any) {
        guard let pass = oldPWTF.text, pass != ""
            else {
                GlobalMethod.init().showAlert(title: "Password", message: "Please enter your old password!", vc: self)
                return
        }
        
        guard let npass = newPWTF.text, npass != ""
            else {
                GlobalMethod.init().showAlert(title: "Password", message: "Please enter your new password!", vc: self)
                return
        }
        
        guard let cpass = confirmPWTF.text, cpass != ""
            else {
                GlobalMethod.init().showAlert(title: "Password", message: "Please enter confirm password!", vc: self)
                return
        }
        
        if npass.count < 8 {
            GlobalMethod.init().showAlert(title: "Password", message: "Password should be at least 8 characters!", vc: self)
        }
        else if npass != cpass {
            GlobalMethod.init().showAlert(title: "Password", message: "Password Mis-Match!", vc: self)

        }
        else {
            self.registerByApi(nPassword: npass, password: pass)
        }
    }
    
}

extension ChangePWVC {
    
    func registerByApi(nPassword: String, password: String) -> Void {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""

        let parameters = [
            "session_id": session,
            "new_password": nPassword,
            "password": password
        ]
        
        Loader.shared.show()
        
        AF.request(REGISTER, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce => ", rss)
        
        if rss["status"].intValue == 200  {
            
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
            
        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
    }
    
}
