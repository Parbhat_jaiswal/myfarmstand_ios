//
//  AddProductVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 23/10/20.
//

import UIKit
import GooglePlaces
import Alamofire
import SwiftyJSON

class AddProductVC: UIViewController {

    @IBOutlet weak var prodImg: UIImageView!
    
    @IBOutlet weak var locationTF: UITextField!
    @IBOutlet weak var prodName: UITextField!
    @IBOutlet weak var prodDescTV: UITextView!
    @IBOutlet weak var prodCat: UITextField!
    
    @IBOutlet weak var prodAmtTF: UITextField!
    var address  = String()
    var lat:String = ""
    var long:String = ""
    var catPicker = UIPickerView()
    var imagePicker = UIImagePickerController()
    var categoryList = NSMutableArray()
    var selectedPicker = Int()
    var catSelectedId = Int()
    var selectedImg: UIImage?
    var prodDescStr = "Product Description"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        catPicker.dataSource = self
        catPicker.delegate = self
        
        prodDescTV.textColor = .lightGray
        prodDescTV.text = prodDescStr
        prodDescTV.delegate = self
        prodDescTV.isHidden = false
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getCategory(isLoading: false)
    }
    
    
    //MARK: - Button Action METHOD

    @IBAction func addImgAction(_ sender: Any) {
        selectImage()
    }
    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func categoryAction(_ sender: Any) {
        if categoryList.count == 0 {
            getCategory(isLoading: true)
        }
        else {
            self.prodCat.inputView = catPicker
            self.prodCat.becomeFirstResponder()
        }
    }
    
    
    @IBAction func locationAction(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

    }
    
    @IBAction func updateAction(_ sender: Any) {
        guard let prodName = prodName.text, prodName != ""
            else {
                GlobalMethod.init().showAlert(title: "Product Name", message: "Please enter your product name!", vc: self)
                return
        }

        guard let prodDesc = prodDescTV.text, prodDesc != ""
            else {
                GlobalMethod.init().showAlert(title: "Product Decription", message: "Please enter your product description!", vc: self)
                return
        }
        
        
        guard let category = prodCat.text, category != ""
            else {
                GlobalMethod.init().showAlert(title: "Product Category", message: "Please enter your product category!", vc: self)
                return
        }

        
        guard let price = prodAmtTF.text, price != ""
            else {
                GlobalMethod.init().showAlert(title: "Product Cost", message: "Please enter your product price!", vc: self)
                return
        }
        
        guard let address = locationTF.text, address != ""
            else {
                GlobalMethod.init().showAlert(title: "Shop Address", message: "Please enter your product's shop address!", vc: self)
                return
        }
        
        
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""
        
        print(selectedImg)
        
        if let data = selectedImg?.jpegData(compressionQuality: 0.5) {
            let parameters = [
                "title": prodName,
                "category": "\(catSelectedId)",
                "description": prodDesc,
                "price": price,
                "currency": "$",
                "address": address,
                "latitude": lat,
                "longitude": long,
                "session_id": session,
            ]
            updateInfo(image: data, parameters: parameters)
        } else {
            GlobalMethod.init().showAlert(title: "Food Image", message: "Please select product image!", vc: self)
        }
        
    }

    fileprivate func loadImage(defaultImage : UIImage?, url : String?, imageView : UIImageView?){
        imageView?.image = defaultImage
        
//        imageLoader.loadImage(url , token: { () -> (Int) in
//            return (self.index ?? 0)
//        }) { (success, image) in
//            if(!success){
//                return
//            }
//            imageView?.image = image
//        }
    }
}



// MARK: - TextView Delegate

extension AddProductVC : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView){
        if textView.text == prodDescStr {
            textView.text = ""
            textView.textColor = .black
            let pb = UIPasteboard.general
            pb.setValue("", forPasteboardType: "general")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView)  {
        if textView.text.isEmpty{
            textView.text = prodDescStr
            textView.textColor = .lightGray
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == prodDescTV   {
            let pb = UIPasteboard.general
            pb.setValue("", forPasteboardType: "general")
            let str = (prodDescTV.text! as NSString).replacingCharacters(in: range, with: text)
            return checkEnglishPhoneNumberFormat1(string: text, str: str)
        }
        else {
            return true
        }
    }
    
    func checkEnglishPhoneNumberFormat1(string: String?, str: String?) -> Bool{
        return true
    }
    
    
}

//MARK: - API METHOD

extension AddProductVC {
    

func updateInfo(image:Data?, parameters: [String : Any]) {
    
    Loader.shared.show()
    
    AF.upload(multipartFormData: { (MultipartFormData) in
        for (key, value) in parameters {
            MultipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
        }
        
        if image != nil {
            MultipartFormData.append(image!, withName: "images", fileName: "image.jpeg", mimeType: "image/jpeg")
        }
    },to: ADD_PRODUCTS, usingThreshold: UInt64.init(),
      method: .post).response { response in
        
        print(response)
        
        switch response.result {
        case .success (let res):
            guard  let r = res else {
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: "Please try again latter!", vc: self)
                })
                return
            }
            
            if JSON(r)["status"].intValue == 200 {
                DispatchQueue.main.async(execute: {
                    GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(r)["message"].stringValue, vc: self)
                })
            } else {
                GlobalMethod.init().showAlert(title: APP_NAME, message: JSON(r)["message"].stringValue, vc: self)
            }
            self.navigationController?.popViewController(animated: true)

            DispatchQueue.main.async(execute: {
                Loader.shared.hide()
            })
                            
        case .failure(let error):
            print(error.localizedDescription)
            DispatchQueue.main.async(execute: {
                Loader.shared.hide()
                GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
            })
        }
        
        DispatchQueue.main.async(execute: {
            Loader.shared.hide()
        })
    }
}
    
    func getCategory(isLoading: Bool) -> Void {
        
        if isLoading == true { Loader.shared.show() }
        
        AF.request(CATEGORY, method:.get, encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success (let res):
                
                if JSON(res)["status"].intValue == 200 {
                    let JSONInfo = JSON(res)["data"].arrayValue
                    self.categoryList.removeAllObjects()
                    for i in JSONInfo {
                        self.categoryList.add(i)
                    }
                    
                    DispatchQueue.main.async(execute: {
                        self.catPicker.reloadAllComponents()
                    })
                    if isLoading {
                        self.prodCat.inputView = self.catPicker
                        self.prodCat.becomeFirstResponder()
                    }
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: "Please try again latter!", vc: self)
                    })
                }
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
}


//MARK: - Delegate METHOD

//MARK:  PICKER DELEGATE METHOD

extension AddProductVC : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categoryList.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let dict = JSON(categoryList[row])
        return "\(dict["category_name"].stringValue)"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let dict = JSON(categoryList[row])
        prodCat.text = "\(dict["category_name"].stringValue)"
        selectedPicker = row
        catSelectedId = dict["id"].intValue
    }
}


// MARK: - Google Place Picker

extension AddProductVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.locationTF.text = String(place.name ?? "")
        address = locationTF.text ?? ""
        lat = String(place.coordinate.latitude)
        long = String(place.coordinate.longitude)
        getAddressFromLatLon(address: place.name ?? "", pdblLatitude: String(place.coordinate.latitude), withLongitude: String(place.coordinate.longitude))

        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func getAddressFromLatLon(address: String, pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        print(lat)
        print(lon)
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                print(placemarks)
                
                if let pm = placemarks {
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        print(pm.country)
                        
                        print(pm.locality)
                        
                        print(pm.subLocality)
                        
                        print(pm.thoroughfare)
                        
                        print("zip code", pm.postalCode)
                        
                        print(pm.subThoroughfare)
                        
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        print(addressString)
                    }
                }
        })
        
    }
    
    
}


extension AddProductVC: UIImagePickerControllerDelegate & UINavigationControllerDelegate {
    
    func selectImage() {
        let alertController = UIAlertController(title: "Perform Action", message: nil, preferredStyle: .actionSheet)
        
        let library = UIAlertAction(title: "Choose From Library", style: .default, handler: { (action) -> Void in
            self.imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            self.imagePicker.allowsEditing = false
            self.present(self.imagePicker, animated: true, completion: nil)
        })
        
        let camera = UIAlertAction(title: "Take From Camera", style: .default, handler: { (action) -> Void in
            
            if !UIImagePickerController.isSourceTypeAvailable(.camera){
                
                let alertController = UIAlertController.init(title: "Alert", message: "Device has no camera!", preferredStyle: .alert)
                
                let okAction = UIAlertAction.init(title: "Ok", style: .default, handler: {(alert: UIAlertAction!) in
                })
                
                alertController.addAction(okAction)
                self.present(alertController, animated: true, completion: nil)
                
            } else {
                self.self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        
        alertController.addAction(library)
//        alertController.addAction(camera)
        alertController.addAction(cancel)
        
        self.navigationController?.present(alertController, animated: true){}
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            print(image)
            self.prodImg.image = image
            selectedImg = self.prodImg.image
            
//            print(img)
            print(selectedImg)

            
//            profileImg.image = image
//
//            if let data = image.jpegData(compressionQuality: 0.5), let session = UserDefaults.standard.value(forKey: "session") as? String {
//                 self.uploadImage(image: data, parameters: ["session_id": session])
//            }
            
        }
        dismiss(animated: true, completion: nil)
    }
    
}
