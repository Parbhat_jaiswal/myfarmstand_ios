//
//  FavoritesVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 22/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

class FavoritesVC: UIViewController {
    @IBOutlet weak var favTable: UITableView!
    
    @IBOutlet weak var noDataLbl: UILabel!
    
    
    var favProducts = [FavouriteModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchFavProdList(isLoading: true)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        fetchFavProdList(isLoading: false)
    }
}
extension FavoritesVC {
    //Add to Favt
    private func fetchFavProdList(isLoading: Bool) -> Void {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { print("no session id found in search API")
            return}
        let parameters = [
            "session_id": session,
        ]
        
        Loader.shared.show()
        
        AF.request(FAVORITES, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    //remove to Favt
    private func removeProductFavt(isLoading: Bool, product_id: String, completion:@escaping(_:Bool?)  -> Void)  {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""
        let parameters = [
            "session_id": session,
            "product_id" : product_id
        ]
        
        Loader.shared.show()

        AF.request(ADD_REMOVE_FAVORITE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
                completion(true)
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
                completion(false)
            }
        }

    }
    private func exterateData(rss: JSON) -> Void {
        
        print("Server Responce  =>  \(rss)")

        if rss["message"].stringValue == "Favourites List Fetched"  {
            favProducts.removeAll()
            for i in rss["data"].arrayValue {
                favProducts.append(FavouriteModel.init(info: JSON(i)))
            }

            DispatchQueue.main.async(execute: {
                self.favTable.reloadData()
                self.favTable.isHidden = self.favProducts.count == 0
                self.noDataLbl.isHidden = !(self.favProducts.count == 0)
            })

        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
}

//MARK: - Declaration of TableView Delegate Methods
extension FavoritesVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoritesCell_Id", for: indexPath) as! FavoritesCell
        let index = favProducts[indexPath.row]

        if index.images.count != 0 {
            let imgUrl = index.images_base_path! + index.images[0].image!
            if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                cell.productImg?.kf.indicatorType = .activity
                cell.productImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
            }
        }
        
        cell.productNameLbl.text = index.title
        cell.prodAddedLBl.text = index.currency! + index.price!
        cell.productDescLbl.text = index.desc
    
        //Add Action
        cell.unFavBtnClosure = { action in
            self.removeProductFavt(isLoading: true, product_id: "\(index.product_id!)") { (val) in
                if val ?? false {
                    self.favProducts.remove(at: indexPath.row)
                    self.favTable.reloadData()
                    self.favTable.isHidden = self.favProducts.count == 0
                    self.noDataLbl.isHidden = !(self.favProducts.count == 0)
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let story =  UIStoryboard.init(name: "Main", bundle: nil)
//        let vc = story.instantiateViewController(withIdentifier: "ChatVC_Id") as! ChatVC
//        vc.userId = JSON(dataSource[indexPath.row])["user_id"].stringValue
//        vc.userName = JSON(dataSource[indexPath.row])["name"].stringValue
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func blockAction(_ sender: UIButton) {
//        let dialogMessage = UIAlertController(title: APP_NAME, message: "Are you sure you want to block?", preferredStyle: .alert)
//        // Create OK button with action handler
//        let ok = UIAlertAction(title: "YES", style: .destructive, handler: { (action) -> Void in
//            self.blockByApi(index: sender.tag, id: JSON(self.dataSource[sender.tag])["id"].stringValue, userName: JSON(self.dataSource[sender.tag])["name"].stringValue)
//        })
//        // Create Cancel button with action handlder
//        let cancel = UIAlertAction(title: "NO", style: .cancel) { (action) -> Void in
//            print("Cancel button tapped")
//        }
//        //Add OK and Cancel button to dialog message
//        dialogMessage.addAction(ok)
//        dialogMessage.addAction(cancel)
//        // Present dialog message to user
//        self.present(dialogMessage, animated: true, completion: nil)
    }
}

class FavoritesCell: UITableViewCell {
    
    
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var productImg: UIImageView!
    
    @IBOutlet weak var productNameLbl: UILabel!
    
    @IBOutlet weak var productDescLbl: UILabel!
    
    @IBOutlet weak var prodAddedLBl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    

    var unFavBtnClosure : ((FavoritesCell) -> Void)?
    @IBAction func removeBtnAction(_ sender: UIButton) {
        unFavBtnClosure?(self)
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}




