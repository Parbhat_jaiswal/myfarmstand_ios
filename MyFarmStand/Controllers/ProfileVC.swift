//
//  ProfileVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 22/10/20.
//

import UIKit

import Alamofire
import SwiftyJSON

import StoreKit

class ProfileVC: UIViewController {
    
    @IBOutlet weak var usernameLBl: UILabel!
    @IBOutlet weak var prodImg: UIImageView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        usernameLBl.text = UserDefaults.standard.string(forKey: "user_name")?.capitalized
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getUserProfile(isLoading: false)
    }
    
    @IBAction func uploadImage(_ sender: UIButton) {
        
    }
    
    //MARK: - Declaration Action Methods
    @IBAction func editAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "EditProfileVC_Id", VC: self)
    }
    
    @IBAction func personalInfoAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "EditProfileVC_Id", VC: self)
    }
    
    @IBAction func changePwAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "ChangePWVC_Id", VC: self)
    }
        
    @IBAction func productAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "MyProductVC_Id", VC: self)
    }
    
    @IBAction func inviteShareAction(_ sender: UIButton) {
        let url = "https://apps.apple.com/us/app/myfarmstand/id1543737574"
        let msg = "Foods is a MarketPlace where anyone can buy food easily and quickly.. Download  FoodsApp from:- \(url)"
        let activityViewController = UIActivityViewController(
            activityItems: [msg],
            applicationActivities: nil)
        if activityViewController.popoverPresentationController != nil {
            //            popoverPresentationController.barButtonItem = (sender as! UIBarButtonItem)
        }
        self.present(activityViewController, animated: true, completion: nil)
    }

    
    @IBAction func helpAction(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LoadWebURLVC_Id") as! LoadWebURLVC
        vc.headerTitle = "Help"
        vc.webURL = ""
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func bookingAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "PurchaseHistoryVC", VC: self)
    }
    
    @IBAction func addProductAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "AddProductVC_Id", VC: self)
    }
    
    @IBAction func termsAndConditionAction(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LoadWebURLVC_Id") as! LoadWebURLVC
        vc.headerTitle = "Terms And Conditions"
        vc.webURL = TERM_CONDITION
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func giveUsFeedBackAction(_ sender: UIButton) {
        if let url = URL(string: "https://apps.apple.com/us/app/myfarmstand/id1543737574") {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func logoutAction(_ sender: UIButton) {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to logged out!", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (alert: UIAlertAction!) -> Void in
            self.logoutByApi()
        }
        let no = UIAlertAction(title: "No", style: .default) { (alert: UIAlertAction!) -> Void in
            
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)
    }
    
}

//MARK: - Declaration API Methods
extension ProfileVC {
    //MARK: - Search API
    func getUserProfile(isLoading: Bool) -> Void {
        
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { return }
        
        guard let id = UserDefaults.standard.string(forKey: "User_Id") else { print("no user_id id found in getUserProfile API")
            return}
        print(UserDefaults.standard.string(forKey: "session_id"))
        let parameters = [
            "user_id": id,
        ]
        
        Loader.shared.show()
        
        AF.request(GET_PROFILE, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.extractSearchData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    func extractSearchData(rss: JSON) -> Void {
        print("Server Responce  =>  \(rss)")
        
        if rss["status"].intValue == 200  {
            let userInfo = rss["profile"].dictionaryValue
            UserDefaults.standard.setValue(userInfo["user_id"]?.stringValue, forKey: "User_Id")
            UserDefaults.standard.setValue(userInfo["user_name"]?.stringValue, forKey: "user_name")
            UserDefaults.standard.setValue(userInfo["email"]?.stringValue, forKey: "email")
            UserDefaults.standard.setValue(userInfo["profile_image"]?.stringValue, forKey: "profile_image")
            UserDefaults.standard.setValue(userInfo["phone"]?.stringValue, forKey: "phone")
            UserDefaults.standard.setValue(userInfo["dob"]?.stringValue, forKey: "dob")
            
            let url = userInfo["profile_image"]?.stringValue
            if let urlString = url?.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
                prodImg?.kf.indicatorType = .activity
                prodImg?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
            }

            
        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
        if rss["error_description"].stringValue == "Your login session has been expired." {
            GlobalMethod.init().removeKey()
            
            let alert = UIAlertController(title: "Success", message: "You have been successfully logged out!", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
                let controllers : Array = self.navigationController!.viewControllers
                self.navigationController!.popToViewController(controllers[0], animated: true)
            }
            alert.addAction(ok)
            self.present(alert, animated: true, completion:nil)
        }
        
    }
    
}


//MARK: - Declaration Logout API Methods

extension ProfileVC {
    
    func logoutByApi() -> Void {
        
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        
        Loader.shared.show()
        
        AF.request(LOGOUT, method:.post, parameters: ["session_id": token], encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success (let res):
                print("LogOut info :-> ", res)
                let JSONInfo = JSON(res)
                if JSONInfo["message"].stringValue == "Logged Out" {
                    DispatchQueue.main.async(execute: {
                        self.logOut()
                    })
                } else if JSONInfo["message"].stringValue == "Invalid Token" {
                    DispatchQueue.main.async(execute: {
                        self.logOut()
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        GlobalMethod.init().showAlert(title: APP_NAME, message: "Please try again latter!", vc: self)
                    })
                }
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                })
                
            case .failure(let error):
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    
    func logOut() -> Void {
        self.removeKey()
        
        let alert = UIAlertController(title: "Success", message: "You have been successfully logged out!", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (alert: UIAlertAction!) -> Void in
            let controllers : Array = self.navigationController!.viewControllers
            self.navigationController!.popToViewController(controllers[0], animated: true)
        }
        alert.addAction(ok)
        self.present(alert, animated: true, completion:nil)
    }
    
    func removeKey() -> Void {
        UserDefaults.standard.removeObject(forKey: "session_id")
        UserDefaults.standard.removeObject(forKey: "User_Id")
        UserDefaults.standard.removeObject(forKey: "user_name")
        UserDefaults.standard.removeObject(forKey: "email")
        UserDefaults.standard.removeObject(forKey: "profile_image")
        
    }
    
}



