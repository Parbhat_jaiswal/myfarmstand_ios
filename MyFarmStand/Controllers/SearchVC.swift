//
//  SearchVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 06/11/20.
//

import UIKit
import Alamofire
import SwiftyJSON
import GoogleMaps
import GooglePlaces

class SearchVC: UIViewController {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var search: UISearchBar!
    var searchProducts = NSMutableArray()
        
    var markerDict: [String: GMSMarker] = [:]
    var marker: GMSMarker?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        if let cancelButton = search.value(forKey: "cancelButton") as? UIButton{
            cancelButton.isEnabled = true
        }
        search.becomeFirstResponder()
        search.delegate = self
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
    }

    @IBAction func goBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func closeSearch() {
        search.resignFirstResponder()
        search.text = ""
        
        UIView.animate(withDuration: 0.6, animations: {
            self.view.layoutIfNeeded()
        })
    }
}


//MARK: - Declaration of Searchbar Delegate Methods
extension SearchVC : UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.closeSearch()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count > 0 {
            self.searchProduct(isLoading: false, str: searchText)
        }
        else {
            self.mapView.clear()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
}


//MARK: - Search API
extension SearchVC {
    func searchProduct(isLoading: Bool, str: String) -> Void {
        guard let session = UserDefaults.standard.string(forKey: "session_id") else { print("no session id found in search API")
            return}
        let parameters = [
            "session_id": session,
            "search" : str
        ]
        
        Loader.shared.show()
        
        AF.request(SEARCH_PRODUCTS, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.extractSearchData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    func extractSearchData(rss: JSON) -> Void {
        print("Server Responce  =>  \(rss)")
        
        
        if rss["status"].intValue == 200  {
            searchProducts.removeAllObjects()
            self.mapView.clear()
            for i in rss["data"].arrayValue {
                searchProducts.add(i)
                
                let data = JSON(i)
                let lat = data["latitude"].doubleValue
                let long = data["longitude"].doubleValue
                let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 6.0)
                self.mapView.camera = camera

                // Creates a marker in the center of the map.
                self.marker = GMSMarker()
                self.marker?.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
                self.marker?.title = data["title"].stringValue + " (" + (data["currency"].stringValue + data["price"].stringValue) + ")"
                self.marker?.snippet = data["title"].stringValue
                self.marker?.map = mapView
                
            }
        }
        print(searchProducts)
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
    }
    
    
}



//MARK: - Map View Delegate Methods

extension SearchVC: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "ProductDetailVC_Id") as! ProductDetailVC
        self.navigationController?.pushViewController(vc, animated: true)
    }

    

}
