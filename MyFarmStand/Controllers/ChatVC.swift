//
//  ChatVC.swift
//  Gays4gals
//
//  Created by Prabhat on 04/09/20.
//  Copyright © 2020 unwrapsolutions. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Kingfisher
import SDWebImage
import IQKeyboardManagerSwift


class ChatVC: UIViewController {
    
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var emptyLbl: UILabel!
    @IBOutlet weak var tablView: UITableView!
    @IBOutlet weak var textMessage: UITextField!
    
    var userName: String = ""
    var prodId : String = ""
    var userId: String = ""
    var dataSource: [ChatModel] = []
    
    var timer = Timer()
    
    var loadOneTime: Bool?
    
    var id = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(prodId)
        
        initilize()
        id = Int(UserDefaults.standard.string(forKey: "User_Id") ?? "-1") ?? -1
    }
    
    func initilize()  {
        loadOneTime = true
        getAllChat(isShowLoader: true)
        
        placeHolder()
        
        titleName.text = userName.capitalized 
        
        tablView.rowHeight = UITableView.automaticDimension
        tablView.estimatedRowHeight = 44
        
        setupViewResizerOnKeyboardShown()
        IQKeyboardManager.shared.enable = false
        
        
        let headerNib = UINib.init(nibName: "ChatHeaderView", bundle: Bundle.main)
        tablView.register(headerNib, forHeaderFooterViewReuseIdentifier: "ChatHeaderView")
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        scheduledTimerWithTimeInterval()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func scheduledTimerWithTimeInterval(){
        timer = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.updateCounting), userInfo: nil, repeats: true)
    }
    
    @objc func updateCounting(){
        getAllChat(isShowLoader: false)
    }
    
    func tableViewScrollToBottom(animated: Bool) {
        DispatchQueue.main.async {
            let numberOfSections = self.tablView.numberOfSections
            let numberOfRows = self.tablView.numberOfRows(inSection: numberOfSections-1)
            
            if numberOfRows > 0 {
                let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                self.tablView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            }
        }
    }
    
    
    func placeHolder() {
        textMessage?.attributedPlaceholder = NSAttributedString(string: "Message....", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
    }

    
    @IBAction func backAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func blockAction(_ sender: UIButton) {
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to report and block this user?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .destructive) { (alert: UIAlertAction!) -> Void in
            self.blockById(id: Int(self.userId) ?? 0)
        }
        let no = UIAlertAction(title: "No", style: .default) { (alert: UIAlertAction!) -> Void in
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)
    }
    
    @IBAction func sendMsgAction(_ sender: UIButton) {
        guard let text = textMessage.text, text != ""
        else {
            GlobalMethod.init().showAlert(title: APP_NAME, message: "Please enter your message!", vc: self)
            return
        }
        self.loadOneTime = true
        sendMessage(msg: text)
    }
    
}


extension ChatVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (textField === textMessage) {
            textMessage.resignFirstResponder()
        }
        return true
    }
    
    func setupViewResizerOnKeyboardShown() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShowForResizing), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHideForResizing), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    @objc func keyboardWillShowForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
           let window = self.view.window?.frame {
            // We're not just minusing the kb height from the view height because
            // the view could already have been resized for the keyboard before
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: window.origin.y + window.height - keyboardSize.height)
        } else {
            debugPrint("We're showing the keyboard and either the keyboard size or window is nil: panic widely.")
        }
    }
    
    @objc func keyboardWillHideForResizing(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let viewHeight = self.view.frame.height
            self.view.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.width, height: viewHeight + keyboardSize.height)
        } else {
            debugPrint("We're about to hide the keyboard and the keyboard size is nil. Now is the rapture.")
        }
    }
}

extension ChatVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if dataSource.count == 0  {
            emptyLbl.isHidden = false
        } else {
            emptyLbl.isHidden = true
        }
        return dataSource.count
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "ChatHeaderView") as! ChatHeaderView
        
        headerView.lblTitle.text = dataSource[section].titleDate
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(section)
        return dataSource[section].msgArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let index = dataSource[indexPath.section].msgArr[indexPath.row]
        
        //        let id = Int(getSting(key: "userId"))
        //
        if index.from_user_id == id {
            let SendCell = tableView.dequeueReusableCell(withIdentifier: "SendCell_Id", for: indexPath) as! SendCell
            SendCell.lbl.text = index.message
            return SendCell
        } else {
            let ReciveCell = tableView.dequeueReusableCell(withIdentifier: "ReciveCell_Id", for: indexPath) as! ReciveCell
            ReciveCell.lbl.text = index.message
            return ReciveCell
        }
    }
    
}


//MARK: - API MEthods
extension ChatVC {
    func getAllChat(isShowLoader: Bool) {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""

        let param = ["session_id": session, "user_id": userId, "old": "0", "page":"1", "limit": "10","product_id":prodId] as [String : Any]
        
        print(param)
        
        if isShowLoader {
            Loader.shared.show()
        }
        AF.request(RECIEVE_MESSAGE, method:.post, parameters:param, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(res)
                self.getChatsResponce(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func getChatsResponce(rss: JSON) -> Void {
        print(rss)
        if rss["status"].intValue == 200  {
            dataSource.removeAll()
            for i in rss["chat_list"].arrayValue {
                dataSource.append(ChatModel.init(info: JSON(i)))
            }
            
        } else if  rss["status_code"].intValue == 403  {
//            GlobalMethod.init().logOut(vc: self)
        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
            self.tablView.reloadData()
            
            if self.dataSource.count != 0 {
                if self.loadOneTime == true {
                    self.tableViewScrollToBottom(animated: false)
                    self.loadOneTime = false
                }
            }
            
        }
        
    }
    
    func deleteMessage(id: String)  {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""

        let params = ["session_id": session, "to_user_id": id] as [String : Any]
        
        Loader.shared.show()
        AF.request(DELETE_CHAT, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                if JSON(res)["message"].stringValue == "Message deleted successfully" {
                    self.navigationController?.popViewController(animated: true)
                }
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func sendMessage(msg: String)  {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""

        let params = ["session_id": session, "user_id": userId, "message": msg,"product_id":prodId] as [String : Any]
        
        
        print(params)
        
        Loader.shared.show()
        AF.request(SEND_MESSAGE, method:.post, parameters:params, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func exterateData(rss: JSON) -> Void {
        print(rss)
        if rss["status"].intValue == 200  {
            DispatchQueue.main.async {
                self.placeHolder()
                self.textMessage.text = nil
                
            }
        } else if  rss["status_code"].intValue == 403  {
//            GlobalMethod.init().logOut(vc: self)
        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
    }
    
    
}

extension ChatVC {
    
    private func blockById(id: Int) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        guard let userId = UserDefaults.standard.value(forKey: "User_Id") as? String else { return }

        let parameters = [
            "user_id":userId,
            "block":1,
            "block_user_id":id
        ] as [String : Any]
        
        print(parameters)
        
        Loader.shared.show()
        
        AF.request(BLOCK_USER, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(JSON(res))
                
                if JSON(res)["status"].intValue == 200 {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: JSON(res)["message"].stringValue, message: "", vc: self)
                        self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: JSON(res)["message"].stringValue, message: "", vc: self)
                        self.navigationController?.popViewController(animated: true)
                    })
                }
                
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}

//MARK: - CELL
class SendCell: UITableViewCell {
    
    @IBOutlet weak var lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}

class ReciveCell: UITableViewCell {
    
    @IBOutlet weak var lbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
