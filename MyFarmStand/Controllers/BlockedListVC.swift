//
//  BlockedListVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 06/02/21.
//

import UIKit
import Alamofire
import SwiftyJSON

class BlockedListVC: UIViewController {

    @IBOutlet weak var tbl: UITableView!
    @IBOutlet weak var noDataLbl: UILabel!
    
    var dataSource = NSMutableArray()

    override func viewDidLoad() {
        super.viewDidLoad()
        getList()
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}

extension BlockedListVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BlockedListCell", for: indexPath) as! BlockedListCell
        let index = JSON(dataSource[indexPath.row])

        cell.name.text = index["user_name"].stringValue.capitalized
        
        let imgUrl = index["profile_image"].stringValue
        if let urlString = imgUrl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) {
            cell.img?.kf.indicatorType = .activity
            cell.img?.kf.setImage(with: URL(string: urlString), placeholder: UIImage.init(named: "LoadingImage"), options: [.transition(.fade(0.7))], progressBlock: nil)
        }
        
        cell.btn.tag = indexPath.row
        cell.btn.addTarget(self, action: #selector(UnBlockBtnAction(_:)), for: .touchUpInside)

        return cell
    }
    
    @objc func UnBlockBtnAction(_ sender: UIButton) {
        let index = JSON(dataSource[sender.tag])
        let id = index["user_id"].intValue
        
        let alert = UIAlertController(title: APP_NAME, message: "Are you sure want to unblock this user?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Yes", style: .default) { (alert: UIAlertAction!) -> Void in
            self.unblockById(id: id)
        }
        let no = UIAlertAction(title: "No", style: .cancel) { (alert: UIAlertAction!) -> Void in
        }
        alert.addAction(yes)
        alert.addAction(no)
        present(alert, animated: true, completion:nil)
    }

    
}

extension BlockedListVC {
    private func getList() -> Void {
        let session = UserDefaults.standard.string(forKey: "session_id") ?? ""
        let id = UserDefaults.standard.string(forKey: "User_Id") ?? ""
        let parameters = [
            "user_id":id,
            "session_id": session,
        ]
        print("SESSION =>>",parameters)
        print("BLOCK_USER_LIST =>>",BLOCK_USER_LIST)
        Loader.shared.show()
        
        AF.request(BLOCK_USER_LIST, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        print("Server Responce  =>  \(rss)")

        if rss["status"].intValue == 200  {
            dataSource.removeAllObjects()
            for i in rss["user_list"].arrayValue {
                dataSource.add(i)
            }
            
            DispatchQueue.main.async(execute: {
                self.tbl.isHidden = self.dataSource.count == 0
                self.noDataLbl.isHidden = !(self.dataSource.count == 0)
                self.tbl.reloadData()
            })

        }
        else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
}

extension BlockedListVC {
    
    private func unblockById(id: Int) -> Void {
        guard let token = UserDefaults.standard.value(forKey: "session_id") as? String else { return }
        guard let userId = UserDefaults.standard.value(forKey: "User_Id") as? String else { return }

        let parameters = [
            "user_id":userId,
            "block":0,
            "block_user_id":id
        ] as [String : Any]
        
        print(parameters)
        
        Loader.shared.show()
        
        AF.request(BLOCK_USER, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                print(JSON(res))
                
                if JSON(res)["status"].intValue == 200 {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: JSON(res)["message"].stringValue, message: "", vc: self)
                        self.navigationController?.popViewController(animated: true)
                    })
                } else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                        GlobalMethod.init().showAlert(title: JSON(res)["message"].stringValue, message: "", vc: self)
                        self.navigationController?.popViewController(animated: true)
                    })
                }
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
}

class BlockedListCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var img: DesignableImage!
    @IBOutlet weak var btn: UIButton!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
