//
//  MainChatVC.swift
//  Art_B
//
//  Created by Ranjit on 05/09/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

class MainChatVC: UIViewController {
    
    @IBOutlet weak var pageScrollView: UIScrollView!
    @IBOutlet weak var myBuddyView: UIView!
    @IBOutlet weak var myMessagesview: UIView!
    
    fileprivate var _popUpVC = UIViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initilization()
    }
    
    func initilization() -> Void {
        selectedTabBarColor(#colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1), #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), false, true)
        pageScrollView?.delegate = self
    }
    
    @IBAction func allUserAction(_ sender: UIButton) {
        pageScrollView?.contentOffset = CGPoint(x: 0, y: 0)
        selectedTabBarColor(#colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1), #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), false, true)
    }
    
    @IBAction func textedUserAction(_ sender: UIButton) {
        if let width = pageScrollView?.frame.size.width {
            pageScrollView?.contentOffset = CGPoint(x: width, y: 0)
        }
        selectedTabBarColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), #colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1), true, false)
    }
    
    @IBAction func blockedListAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "BlockedListVC", VC: self)
    }
    
}

extension MainChatVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        var currentpage: Int? = nil
        let pageWidth: CGFloat = scrollView.frame.size.width
        var page: Int = Int(floor((scrollView.contentOffset.x) / pageWidth))
        page += 1
        if currentpage != page {
            currentpage = page
            if currentpage == 0 {
                selectedTabBarColor(#colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1), #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), false, true)
            } else if currentpage == 2 {
                selectedTabBarColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), #colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1), true, false)
            }else {
                selectedTabBarColor(#colorLiteral(red: 0.5411764706, green: 0.7764705882, blue: 0.2470588235, alpha: 1), #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), false, true)
            }
        }
    }
    
    func selectedTabBarColor(_ sellingView: UIColor, _ soldView:UIColor, _ sellingBool: Bool, _ soldBool: Bool) -> Void {
        myBuddyView?.backgroundColor = sellingView
        myMessagesview?.backgroundColor = soldView
        
        myBuddyView?.isHidden = sellingBool
        myMessagesview?.isHidden = soldBool
    }
    
}
