//
//  Loader.swift
//  FilterApp
//
//  Created by Prabhat on 27/01/20.
//  Copyright © 2020 Rsoft. All rights reserved.
//

import Foundation
import NVActivityIndicatorView


class Loader: UIViewController, NVActivityIndicatorViewable {
    
   static var shared = Loader()
    
    func show() {
        let activityData = ActivityData(size: nil, message: nil, messageFont: nil, messageSpacing: nil, type: .ballGridPulse, color: #colorLiteral(red: 0.5498781204, green: 0.7718415856, blue: 0.2442100942, alpha: 1), padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: nil)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    func hide() {
        stopAnimating()
    }
    
}
