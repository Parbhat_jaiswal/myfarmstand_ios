//
//  LoadWebURLVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 20/10/20.
//

import UIKit
import WebKit

class LoadWebURLVC: UIViewController, WKNavigationDelegate {

    
    @IBOutlet weak var titleLbl: UILabel?
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    
    @IBOutlet weak var webV: UIView!
    var headerTitle: String?
    var webURL: String?
    
    var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        initilization()
    }
    
    func initilization() -> Void {
        titleLbl?.text = headerTitle
        activityIndicator?.startAnimating()
        guard let strURL = webURL else { return }

        // loading URL :
        let myBlog = strURL
        let url = NSURL(string: myBlog)
        

        let request = NSURLRequest(url: url! as URL)
        
        // init and load request in webview.
        webView = WKWebView(frame: self.view.frame)
        webView.navigationDelegate = self
        webView.load(request as URLRequest)
        self.webV.addSubview(webView)
        self.webV.sendSubviewToBack(webView)
//
//        let myURL = URL(string:strURL)
//        let myRequest = URLRequest(url: myURL!)
//        webView.load(myRequest)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicator?.stopAnimating()
    }
    
//    func webViewDidFinishLoad(_ webView: UIWebView) {
//        activityIndicator?.stopAnimating()
//        activityIndicator?.isHidden = true
//    }
//
//    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
//        activityIndicator?.stopAnimating()
//        activityIndicator?.isHidden = true
//    }
//
    
    @IBAction func goBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
        
    
    //MARK:- WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        activityIndicator?.stopAnimating()
        activityIndicator?.isHidden = true
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        activityIndicator?.startAnimating()
        activityIndicator?.isHidden = false
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator?.stopAnimating()
        activityIndicator?.isHidden = true
    }

    
    
}
