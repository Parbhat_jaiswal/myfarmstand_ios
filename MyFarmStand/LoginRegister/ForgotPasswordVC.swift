//
//  ForgotPasswordVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 20/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ForgotPassword {
    func removePopUp()
}


class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var emailAddress: UITextField!
    
    var delegate: ForgotPassword?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // phoneOrEmailAddress.text = "pj@gmail.com"
    }
        
    @IBAction func continueAction(_ sender: Any) {
        guard let email = emailAddress.text, email != ""
            else {
                GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your email address!", vc: self)
                return
        }
        
        if !(email.isValidEmail) {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Your email address is incorrect!", vc: self)
        } else {
            self.forgotPass(isLoading: true)
        }

    }
    
    @IBAction func crossAction(_ sender: Any) {
        delegate?.removePopUp()
    }
}


extension ForgotPasswordVC {
    //Fetch Popular
    private func forgotPass(isLoading: Bool) -> Void {

        let parameters = [
            "email": emailAddress.text!,
        ]
        
        Loader.shared.show()
        
        AF.request(FORGOT_PW, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    private func exterateData(rss: JSON) -> Void {
        print("Server Responce  =>  \(rss)")

        if rss["status"].intValue == 200  {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["message"].string ?? "Please try after sometime!", vc: self)
            })
            delegate?.removePopUp()
        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }

}
