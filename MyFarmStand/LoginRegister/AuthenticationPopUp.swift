//
//  AuthenticationPopUp.swift
//  uSnapp
//
//  Created by Ranjit on 04/05/18.
//  Copyright © 2018 RichestSoft. All rights reserved.
//

import UIKit

protocol AuthenticationViewProtocal {
    func closeView()
}

class AuthenticationPopUp: UIViewController {
    
    var delegate: AuthenticationViewProtocal?
    var popUpView =  UIViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func goBack(_ sender: UIButton) {
        //NotificationCenter.default.removeObserver(self, name: .authonticatonPopUp, object: nil)
        delegate?.closeView()
    }
    
    @IBAction func register(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        if let vc = story.instantiateViewController(withIdentifier: "RegisterVC_Id") as? RegisterVC {
            self.navigationController?.pushViewController(vc, animated: true)
            delegate?.closeView()
        }
    }
    
    @IBAction func logIn(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        if let vc = story.instantiateViewController(withIdentifier: "LoginVC_Id") as? LoginVC {
            self.navigationController?.pushViewController(vc, animated: true)
            delegate?.closeView()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
