//
//  RegisterVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 20/10/20.
//

import UIKit

import Alamofire
import SwiftyJSON
import GooglePlaces


class RegisterVC: UIViewController {
    
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var location: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    var address  = String()
    var lat:String = ""
    var long:String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func termsConditionAction(_ sender: UIButton) {
        
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LoadWebURLVC_Id") as! LoadWebURLVC
        vc.headerTitle = "Terms And Conditions"
        vc.webURL = TERM_CONDITION
        navigationController?.pushViewController(vc, animated: true)

        
    }
    
    @IBAction func privacyPolicyAction(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LoadWebURLVC_Id") as! LoadWebURLVC
        vc.headerTitle = "Privacy Policy"
        vc.webURL = PRIVACY_POLICY
        navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func moveToLoginAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)

    }
    
    
    @IBAction func signUpAction(_ sender: UIButton) {

        guard let name = username.text, name != ""
            else {
                GlobalMethod.init().showAlert(title: "Username", message: "Please enter your user name!", vc: self)
                return
        }
        
        
        guard let e = email.text, e != ""
            else {
                GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your email address!", vc: self)
                return
        }
    

        guard let pass = password.text, pass != ""
            else {
                GlobalMethod.init().showAlert(title: "Password", message: "Please enter your password!", vc: self)
                return
        }
        
        if !(e.isValidEmail) {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Your email address is incorrect!", vc: self)
        } else if pass.count < 8 {
            GlobalMethod.init().showAlert(title: "Password", message: "Password should be at least 8 characters!", vc: self)
        } else {
            registerByApi(name: name, email: e, loaction: address, password: pass)
        }
        
    }

}

extension RegisterVC {
    
    func registerByApi(name: String, email: String, loaction: String, password: String) -> Void {

        let parameters = [
            "user_name": name,
            "email": email,
            "address": loaction,
            "latitude": lat,
            "longitude": long,
            "password": password,
            "device_type": "2",
            "device_id": "12345678",
            "device_token": UserDefaults.standard.value(forKey: "FCM_TOKEN") ?? "12345",
        ]
        
        Loader.shared.show()
        
        AF.request(REGISTER, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func exterateData(rss: JSON) -> Void {
        
        print("Server Responce => ", rss)
        
        if rss["error_description"].stringValue == "User Registered Successfully."  {
            guard let userInfo = rss["profile"].dictionary
                else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                    })
                    return
            }
            UserDefaults.standard.setValue(rss["session_id"].stringValue, forKey: "session_id")
            UserDefaults.standard.setValue(userInfo["user_id"]?.stringValue, forKey: "User_Id")
            UserDefaults.standard.setValue(userInfo["user_name"]?.stringValue, forKey: "user_name")
            UserDefaults.standard.setValue(userInfo["email"]?.stringValue, forKey: "email")
            UserDefaults.standard.setValue(userInfo["profile_image"]?.stringValue, forKey: "profile_image")
            UserDefaults.standard.setValue(userInfo["phone"]?.stringValue, forKey: "phone")
            UserDefaults.standard.setValue(userInfo["dob"]?.stringValue, forKey: "dob")
            
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "ContainerVC_Id", VC: self)
            })
            
        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
}


// MARK: - Google Place Picker

extension RegisterVC: GMSAutocompleteViewControllerDelegate {
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        location.text = String(place.name ?? "")
        address = location.text ?? ""
        lat = String(place.coordinate.latitude)
        long = String(place.coordinate.longitude)
        getAddressFromLatLon(address: place.name ?? "", pdblLatitude: String(place.coordinate.latitude), withLongitude: String(place.coordinate.longitude))

        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    func getAddressFromLatLon(address: String, pdblLatitude: String, withLongitude pdblLongitude: String) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        let lat: Double = Double("\(pdblLatitude)")!
        //21.228124
        let lon: Double = Double("\(pdblLongitude)")!
        print(lat)
        print(lon)
        //72.833770
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil) {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                
                print(placemarks)
                
                if let pm = placemarks {
                    if pm.count > 0 {
                        let pm = placemarks![0]
                        
                        print(pm.country)
                        
                        print(pm.locality)
                        
                        print(pm.subLocality)
                        
                        print(pm.thoroughfare)
                        
                        print("zip code", pm.postalCode)
                        
                        print(pm.subThoroughfare)
                        
                        var addressString : String = ""
                        if pm.subLocality != nil {
                            addressString = addressString + pm.subLocality! + ", "
                        }
                        if pm.thoroughfare != nil {
                            addressString = addressString + pm.thoroughfare! + ", "
                        }
                        if pm.locality != nil {
                            addressString = addressString + pm.locality! + ", "
                        }
                        if pm.country != nil {
                            addressString = addressString + pm.country! + ", "
                        }
                        if pm.postalCode != nil {
                            addressString = addressString + pm.postalCode! + " "
                        }
                        print(addressString)
                    }
                }
        })
        
    }
    
    
}
