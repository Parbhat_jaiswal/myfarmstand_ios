//
//  LoginVC.swift
//  MyFarmStand
//
//  Created by Prabhat on 20/10/20.
//

import UIKit
import Alamofire
import SwiftyJSON

class LoginVC: UIViewController {

    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    fileprivate var _popUpVC = UIViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
//         email.text = "demo@gmail.com" // testflight
//        email.text = "p@gmail.com"
        
//        email.text = "t@gmail.com"
//         email.text = "test2@mailinator.com" //"prabhat@gmail.com"
//        password.text = "12345678"
        
        if let _ = UserDefaults.standard.value(forKey: "session_id") {
            let story =  UIStoryboard.init(name: "Main", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "ContainerVC_Id") as! ContainerVC
            navigationController?.pushViewController(vc, animated: false)
        }
        
    }
    
    @IBAction func termsConditionAction(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LoadWebURLVC_Id") as! LoadWebURLVC
        vc.headerTitle = "Terms And Conditions"
        vc.webURL = TERM_CONDITION
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyPolicyAction(_ sender: UIButton) {
        let story =  UIStoryboard.init(name: "Main", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "LoadWebURLVC_Id") as! LoadWebURLVC
        vc.headerTitle = "Privacy Policy"
        vc.webURL = PRIVACY_POLICY
        navigationController?.pushViewController(vc, animated: true)

    }
    
    @IBAction func moveToSignUpAction(_ sender: UIButton) {
        GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "RegisterVC_Id", VC: self)
    }
    
    @IBAction func loginAction(_ sender: UIButton) {
        
        guard let e = email.text, e != ""
            else {
                GlobalMethod.init().showAlert(title: "Email Address", message: "Please enter your email address!", vc: self)
                return
        }
        
        guard let password = password.text, password != ""
            else {
                GlobalMethod.init().showAlert(title: "Password", message: "Please enter your password!", vc: self)
                return
        }
        
        if !(e.isValidEmail) {
            GlobalMethod.init().showAlert(title: "Email Address", message: "Your email address is incorrect!", vc: self)
        } else if password.count < 8 {
            GlobalMethod.init().showAlert(title: "Password", message: "Password should be at least 8 characters!", vc: self)
        } else {
            loginUserByApi(email: e, password: password)
        }

    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        if !(_popUpVC.isViewLoaded){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let loadVC = storyboard.instantiateViewController(withIdentifier: "ForgotPasswordVC_Id") as! ForgotPasswordVC
            loadVC.delegate = self
            self.addChild(loadVC)
            self.view.addSubview(loadVC.view)
            loadVC.view.alpha = 0
            
            UIView.animate(withDuration: 0.25, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                loadVC.view.alpha = 1
            }, completion: nil)
            
            _popUpVC = loadVC
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension LoginVC: ForgotPassword {
    func removePopUp() {
        if (_popUpVC.isViewLoaded) {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveLinear, animations: {
                self._popUpVC.view.alpha = 0
            }, completion: nil)
            _popUpVC.view.removeFromSuperview()
            _popUpVC = UIViewController()
        }
    }
}


extension LoginVC {
    
    func loginUserByApi(email: String, password: String) -> Void {
        
        let parameters = [
            "email": email,
            "password": password,
            "device_type":"2",
            "device_id": "12345678",
            "device_token": UserDefaults.standard.value(forKey: "FCM_TOKEN") ?? "12345",
        ]
        
        Loader.shared.show()
        
        AF.request(LOGIN, method:.post, parameters:parameters, encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success (let res):
                self.exterateData(rss: JSON(res))
            case .failure(let error):
                print(error.localizedDescription)
                DispatchQueue.main.async(execute: {
                    Loader.shared.hide()
                    GlobalMethod.init().showAlert(title: APP_NAME, message: error.localizedDescription, vc: self)
                })
            }
        }
    }
    
    func exterateData(rss: JSON) -> Void {
        
        print("Login Info =>  \(rss)")
        
        if rss["message"].stringValue == "User Logged In"  {
            guard let userInfo = rss["profile"].dictionary
                else {
                    DispatchQueue.main.async(execute: {
                        Loader.shared.hide()
                    })
                    return
            }
                        
            UserDefaults.standard.setValue(rss["session_id"].stringValue, forKey: "session_id")
            UserDefaults.standard.setValue(userInfo["user_id"]?.stringValue, forKey: "User_Id")
            UserDefaults.standard.setValue(userInfo["user_name"]?.stringValue, forKey: "user_name")
            UserDefaults.standard.setValue(userInfo["email"]?.stringValue, forKey: "email")
            UserDefaults.standard.setValue(userInfo["profile_image"]?.stringValue, forKey: "profile_image")
            UserDefaults.standard.setValue(userInfo["phone"]?.stringValue, forKey: "phone")
            UserDefaults.standard.setValue(userInfo["dob"]?.stringValue, forKey: "dob")

            DispatchQueue.main.async(execute: {
                GlobalMethod.init().pushToVC(storyBoard: "Main", VCId: "ContainerVC_Id", VC: self)
            })

        } else {
            DispatchQueue.main.async(execute: {
                GlobalMethod.init().showAlert(title: APP_NAME, message: rss["error_description"].string ?? "Please try after sometime!", vc: self)
            })
        }
        
        DispatchQueue.main.async {
            Loader.shared.hide()
        }
        
    }
    
}
